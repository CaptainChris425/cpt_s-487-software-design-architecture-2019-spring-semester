﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PeaShooterGame.Zombies
{
    public class StandardZombieFactory : ZombieFactory
    {
        public override Zombie GetZombie(string zombie)
        {
            Zombie z = new StandardZombie();
            switch (zombie)
            {
                case "BUCKET": case "Bucket": case "bucket":
                    z.Add(new Bucket());
                    break;
                case "CONE": case "Cone": case "cone":
                    z.Add(new Cone());
                    break;
                case "SCREEN": case "Screen": case "screen":
                case "ScreenDoor": case "SCREENDOOR": case "screendoor":
                    z.Add(new ScreenDoor());
                    break;
                case "STANDARD": case "Standard": case "standard":
                    break;
                default:
                    return null;
            }
            return z;
        }
    }
}
