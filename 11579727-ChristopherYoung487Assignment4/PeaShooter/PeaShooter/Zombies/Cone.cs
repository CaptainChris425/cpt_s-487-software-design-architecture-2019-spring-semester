﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PeaShooterGame.Zombies
{
    public class Cone : Zombie
    {
        public Cone()
        {
            Health = 25;
            Alive = true;
            Name = "Cone Zombie";
        }

        public override int Health { get => health; set => health = value;  }

        public override void Add(Zombie zombie)
        {
            throw new NotImplementedException();
        }

        public override void Die()
        {
            Alive = false;
        }

        public override string Draw()
        {
            StringBuilder draw = new StringBuilder();
            draw.Append(",=/*.\\-");
            draw.Append(Health+50);
            draw.Append(" -,");
            return draw.ToString();
        }

        public override Zombie GetChild(int i)
        {
            throw new NotImplementedException();
        }

        public override void Remove(Zombie zombie)
        {
            throw new NotImplementedException();
        }

        public override int TakeDamage(int d)
        {
            Health -= d;
            if (Health <= 0)
            {
                Die();
                return -Health;
            }
            return 0;
        }
    }
}
