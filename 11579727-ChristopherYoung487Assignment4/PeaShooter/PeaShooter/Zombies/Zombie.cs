﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PeaShooterGame.Zombies
{
    public abstract class Zombie
    {
        /*Methods to be overridden by concrete zombies*/
        public bool Alive { get; set; }                 //Active status
        protected int health;                           //Health value determines activeness
        public abstract int Health { get; set; }        //Health get/set
        public string Name { get; set; }                //Name value
        public abstract string Draw();                  //Draw function
        public abstract void Add(Zombie zombie);        //Add to list
        public abstract void Remove(Zombie zombie);     //Remove from list
        public abstract Zombie GetChild(int i);         //Get child at index
        public abstract int TakeDamage(int d);          //Apply damage to zombie
        public abstract void Die();                     //Set to inactive
    }
}
