﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PeaShooterGame.Maps
{
    public abstract class Map
    {
        public abstract void RunMap();
        public abstract void SetUp();
    }
}
