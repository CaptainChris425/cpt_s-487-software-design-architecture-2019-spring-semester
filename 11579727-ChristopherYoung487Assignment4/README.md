____________________________________________________________________
|___________A game by Christopher Young____________________________|
   _____ _          _  __      __   _____      _                 _ 
  / ____| |        (_) \ \    / /  / ____|    | |               | |
 | |    | |__  _ __ _ __\ \  / /__| (___   ___| |__   ___   ___ | |
 | |    | '_ \| '__| / __\ \/ / __|\___ \ / __| '_ \ / _ \ / _ \| |
 | |____| | | | |  | \__ \\  /\__ \____) | (__| | | | (_) | (_) | |
  \_____|_| |_|_|  |_|___/ \/ |___/_____/ \___|_| |_|\___/ \___/|_|
____________________________________________________________________
|___________________________________________________________________|

1.1:                                                                 
Menu:
	Options (Type the corresponding number to choose):
		1. Play through the game you created				(1.2)
		2. Go into a sandbox mode where you choose the:		(1.3)
					*Damage
					*Enemies

1.2
Play Demo
	Simulate a plant
	Choose between: 
		1. Make one shot
		2. Finish Game


1.3
Sandbox Mode
	Allows you to choose the damage of your gun
	Allows you to choose between the various zombies in the game
		1. Regular Zombie - 50 Health
		2. Cone Zombie - 75 Health
		3. Bucket Zombie - 150 Health
		4. ScreenDoor Zombie - 75 Health

	After selecting your unique options:
		Go back to the main menu and play demo

