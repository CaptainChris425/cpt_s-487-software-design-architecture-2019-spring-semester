﻿using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Entity;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Builder
{
    //The director class of the builder pattern
        //This class does the building of the entity
            //It takes in a game for its constructer (to use the content and graphics managers)
            //Then using its construct method
                //Parameter is a concrete IEntityBuilder
                //the director calls the build function on all the parts
                //then returns the built entity.
    class EntityDirector
    {
        protected Game _game;

        public EntityDirector(Game game)
        {
            _game = game;
        }

        public void Construct(IEntityBuilder builder, Vector2 position)
        {
            //For all parts in the object
            //builder.BuildPart
            builder.Initialize(_game);
            builder.BuildActive();
            builder.BuildHealth();
            builder.BuildPosition(position);
            builder.BuildRateOfBullets();
            builder.BuildSpeed();
            builder.BuildTexture(_game.Content);
            builder.BuildViewPortHeight(_game.GraphicsDevice);
            builder.BuildViewPortWidth(_game.GraphicsDevice);
            builder.BuildMovement();
            builder.BuildShooter();
            builder.BuildDamage();
            builder.BuildEntityDirector(_game);
        }
    }
}
