﻿using CptS_487_game.Interfaces.Entity;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using CptS_487_game.Objects;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.MovementFactory;
using CptS_487_game.Interfaces;

namespace CptS_487_game.Builder
{
    class PlayerBuilder : IEntityBuilder
    {
        ConcreteEntity entity;
        MovementCreator movementFactory;
        ShooterFactory shooterFactory;
        //NewPlayer entity; //Entity to be built.

        public PlayerBuilder()
        {
            entity = new ConcreteEntity(); //Instance the entity.
            movementFactory = new MovementCreator();
            shooterFactory = new ShooterFactory();
        }

        public void BuildHealth()
        {
            entity.Health = 3;  //Players get 100 health
        }

        public void BuildPosition(Vector2 position)
        {
            entity.Position = position;
            ////Player position starts at the middle of the bottom of the screen
            //        //Create a new vector that has coordinates based off the game viewport
            //entity.AddPosition(new Vector2(device.Viewport.TitleSafeArea.X + device.Viewport.TitleSafeArea.Width / 2,
            //    device.Viewport.TitleSafeArea.Y + 3 * device.Viewport.TitleSafeArea.Height / 2));
        }

        public void BuildRateOfBullets()
        {
            entity.RateOfBullets = TimeSpan.FromSeconds(60f / 400f); //A player shoots bullets every 60/400 seconds
        }

        public void BuildSpeed()
        {
            entity.Speed = 4; //Players position changes by 4 per second
        }

        public void BuildTexture(ContentManager device)
        {
            entity.DamagesEnemies = false;
            entity.Texture = device.Load<Texture2D>("spritelib_plane"); //The player texture is the spritelib_plane obtained from the game contentmanager
        }

        public void BuildActive()
        {
            entity.Active = true; //We are building it so it is active
        }

        public void BuildViewPortHeight(GraphicsDevice device)
        {
            entity.ViewPortHeight = device.Viewport.Height; //Viewport height set from the games graphics viewport height
        }

        public void BuildViewPortWidth(GraphicsDevice device)
        {
            entity.ViewPortWidth = device.Viewport.Width; //Viewport width set from the games graphics viewport width
        }

        public ConcreteEntity GetResult()
        {
            return entity; //Return the built entity
        }

        public void BuildMovement()
        {
            entity.Movable = movementFactory.CreateProduct("player");
        }

        public void Initialize(Game game)
        {
            entity.Initialize(game);
        }

        public void BuildShooter()
        {
            entity.Shooter = shooterFactory.CreateProduct("player");
        }

        public void BuildDamage()
        {
            entity.Damage = 0;
        }

        public void BuildEntityDirector(Game game)
        {
            entity.EntityDirector = new EntityDirector(game);
        }
    }
}
