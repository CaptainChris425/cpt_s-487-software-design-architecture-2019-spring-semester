﻿using CptS_487_game.Control;
using CptS_487_game.Interfaces;
using CptS_487_game.MovementFactory;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Builder
{
    class EnemyBulletBuilder : IEntityBuilder
    {
        ConcreteEntity entity;
        MovementCreator movementFactory;
        ShooterFactory shooterFactory;

        public EnemyBulletBuilder()
        {
            entity = new ConcreteEntity(); //Instance the entity.
            movementFactory = new MovementCreator();
            shooterFactory = new ShooterFactory();
            entity.End = 9999;
        }

        public void BuildActive()
        {
            entity.Active = true;
        }

        public void BuildDamage()
        {
            entity.Damage = 1*GameValues.GetInstance().Difficulty;
        }

        public void BuildHealth()
        {
            entity.Health = 1000000;
        }

        public void BuildMovement()
        {
            entity.Movable = movementFactory.CreateProduct("enemybullet");
        }

        public void BuildPosition(Vector2 position)
        {
            entity.Position = position;
        }

        public void BuildRateOfBullets()
        {
            entity.RateOfBullets = TimeSpan.FromSeconds(0f);
        }

        public void BuildShooter()
        {
            entity.DamagesEnemies = false;
            entity.Shooter = shooterFactory.CreateProduct("null");
        }

        public void BuildSpeed()
        {
            entity.Speed = 6 * GameValues.GetInstance().Difficulty;
        }

        public void BuildTexture(ContentManager device)
        {
            entity.Texture = device.Load<Texture2D>("spritelib_bullet");
        }

        public void BuildViewPortHeight(GraphicsDevice device)
        {
            entity.ViewPortHeight = device.Viewport.Height; //Viewport height set from the games graphics viewport height
        }

        public void BuildViewPortWidth(GraphicsDevice device)
        {
            entity.ViewPortWidth = device.Viewport.Width; //Viewport width set from the games graphics viewport width
        }

        public ConcreteEntity GetResult()
        {
            return entity;
        }

        public void Initialize(Game game)
        {
            entity.Initialize(game);
        }

        public void BuildEntityDirector(Game game)
        {
            entity.EntityDirector = new EntityDirector(game);
        }
    }
}
