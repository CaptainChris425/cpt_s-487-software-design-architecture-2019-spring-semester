﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces;
using CptS_487_game.MovementFactory;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace CptS_487_game.Builder
{
    class PlayerBulletBuilder : IEntityBuilder
    {
        ConcreteEntity entity;
        MovementCreator movementFactory;
        ShooterFactory shooterFactory;

        public PlayerBulletBuilder()
        {
            entity = new ConcreteEntity(); //Instance the entity.
            movementFactory = new MovementCreator();
            shooterFactory = new ShooterFactory();
        }

        public void BuildActive()
        {
            entity.Active = true;
        }

        public void BuildDamage()
        {
            entity.Damage = 1;
        }

        public void BuildHealth()
        {
            entity.Health = 1000000;
        }

        public void BuildMovement()
        {
            entity.Movable = movementFactory.CreateProduct("playerbullet");
        }

        public void BuildPosition(Vector2 position)
        {
            entity.Position = position;
        }

        public void BuildRateOfBullets()
        {
            entity.RateOfBullets = TimeSpan.FromSeconds(0f);
        }

        public void BuildShooter()
        {
            entity.DamagesEnemies = true;
            entity.Shooter = shooterFactory.CreateProduct("null");
        }

        public void BuildSpeed()
        {
            entity.Speed = 12;
        }

        public void BuildTexture(ContentManager device)
        {
            entity.Texture = device.Load<Texture2D>("spritelib_player_bullet");
        }

        public void BuildViewPortHeight(GraphicsDevice device)
        {
            entity.ViewPortHeight = device.Viewport.Height; //Viewport height set from the games graphics viewport height
        }

        public void BuildViewPortWidth(GraphicsDevice device)
        {
            entity.ViewPortWidth = device.Viewport.Width; //Viewport width set from the games graphics viewport width
        }

        public ConcreteEntity GetResult()
        {
            return entity;
        }

        public void Initialize(Game game)
        {
            entity.Initialize(game);
        }

        public void BuildEntityDirector(Game game)
        {
            entity.EntityDirector = new EntityDirector(game);
        }
    }
}
