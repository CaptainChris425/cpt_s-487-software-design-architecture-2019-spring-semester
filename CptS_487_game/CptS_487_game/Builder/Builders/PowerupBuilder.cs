﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces;
using CptS_487_game.MovementFactory;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace CptS_487_game.Builder
{
    class PowerupBuilder : IEntityBuilder
    {
        ConcreteEntity entity;
        MovementCreator movementFactory;
        ShooterFactory shooterFactory;
        Random random;

        public PowerupBuilder()
        {
            entity = new ConcreteEntity(); //Instance the entity.
            movementFactory = new MovementCreator();
            shooterFactory = new ShooterFactory();
            random = new Random();
        }

        public void BuildActive()
        {
            entity.Active = true;
        }

        public void BuildDamage()
        {
            entity.Damage = 0;
        }

        public void BuildEntityDirector(Game game)
        {
            entity.EntityDirector = new EntityDirector(game);
        }

        public void BuildHealth()
        {
            entity.Health = 9999;
        }

        public void BuildMovement()
        {
            entity.Movable = movementFactory.CreateProduct("stayput");
        }

        public void BuildPosition(Vector2 position)
        {
            entity.Position = position;
        }

        public void BuildRateOfBullets()
        {
            entity.RateOfBullets = TimeSpan.FromSeconds(0);
        }

        public void BuildShooter()
        {
            entity.DamagesEnemies = false;
            entity.Shooter = shooterFactory.CreateProduct("null");
        }

        public void BuildSpeed()
        {
            entity.Speed = 0;
        }

        public void BuildTexture(ContentManager device)
        {
            entity.Texture = device.Load<Texture2D>("ship2");
        }

        public void BuildViewPortHeight(GraphicsDevice device)
        {
            entity.ViewPortHeight = device.Viewport.Height; //Viewport height set from the games graphics viewport height
        }

        public void BuildViewPortWidth(GraphicsDevice device)
        {
            entity.ViewPortWidth = device.Viewport.Width; //Viewport width set from the games graphics viewport width
        }

        public ConcreteEntity GetResult()
        {
            return entity;
        }

        public void Initialize(Game game)
        {
            entity.Initialize(game);
        }
    }
}
