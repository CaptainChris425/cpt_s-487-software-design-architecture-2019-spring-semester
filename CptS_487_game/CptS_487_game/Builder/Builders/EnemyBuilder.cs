﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces;
using CptS_487_game.MovementFactory;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using CptS_487_game.Control;

namespace CptS_487_game.Builder
{
    class EnemyBuilder : IEntityBuilder
    {
        ConcreteEntity entity;
        MovementCreator movementFactory;
        ShooterFactory shooterFactory;
        //NewPlayer entity; //Entity to be built.
        Random random;

        public EnemyBuilder()
        {
            entity = new ConcreteEntity(); //Instance the entity.
            movementFactory = new MovementCreator();
            shooterFactory = new ShooterFactory();
            random = new Random();
        }

        public void BuildActive()
        {
            entity.Active = true;
        }

        public void BuildDamage()
        {
            entity.Damage = 1;
        }

        public void BuildEntityDirector(Game game)
        {
            entity.EntityDirector = new EntityDirector(game);
        }

        public void BuildHealth()
        {
            entity.Health = 5 * GameValues.GetInstance().Difficulty;
        }

        public void BuildMovement()
        {
            entity.Movable = movementFactory.CreateProduct("enemystraight");
        }

        public void BuildPosition(Vector2 position)
        {
            entity.Position = position;
            //int x = random.Next(0,120);
            //entity.Position(new Vector2(x, 0));
        }

        public void BuildRateOfBullets()
        {
            entity.RateOfBullets = TimeSpan.FromSeconds(60f / 400f);
        }

        public void BuildShooter()
        {
            entity.DamagesEnemies = false;
            entity.Shooter = shooterFactory.CreateProduct("enemy");
        }

        public void BuildSpeed()
        {
            entity.Speed = 2 * GameValues.GetInstance().Difficulty;
        }

        public void BuildTexture(ContentManager device)
        {
            entity.Texture = device.Load<Texture2D>("spritelib_orange_plane");
        }

        public void BuildViewPortHeight(GraphicsDevice device)
        {
            entity.ViewPortHeight = device.Viewport.Height;
        }

        public void BuildViewPortWidth(GraphicsDevice device)
        {
            entity.ViewPortHeight = device.Viewport.Width;
        }

        public ConcreteEntity GetResult()
        {
            return entity;
        }

        public void Initialize(Game game)
        {
            entity.Initialize(game);
        }
    }
}
