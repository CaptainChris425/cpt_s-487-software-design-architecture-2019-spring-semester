﻿using CptS_487_game.Interfaces;
using CptS_487_game.MovementFactory;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Builder
{
    class EnemyBulletBuilderWave : IEntityBuilder
    {
        ConcreteEntity entity;
        MovementCreator movementFactory;
        ShooterFactory shooterFactory;

        public EnemyBulletBuilderWave()
        {
            entity = new ConcreteEntity(); //Instance the entity.
            movementFactory = new MovementCreator();
            shooterFactory = new ShooterFactory();
        }

        public void BuildActive()
        {
            entity.AddActive(true);
        }

        public void BuildDamage()
        {
            entity.AddDamage(25);
        }

        public void BuildHealth()
        {
            entity.AddHealth(1000000);
        }

        public void BuildMovement()
        {
            entity.AddMovement(movementFactory.CreateProduct("enemybulletwave"));
        }

        public void BuildPosition(Vector2 position)
        {
            entity.AddPosition(position);
        }

        public void BuildRateOfBullets()
        {
            entity.AddRateOfBullets(TimeSpan.FromSeconds(0f));
        }

        public void BuildShooter()
        {
            entity.AddShooter(shooterFactory.CreateProduct("null"));
        }

        public void BuildSpeed()
        {
            entity.AddSpeed(6);
        }

        public void BuildTexture(ContentManager device)
        {
            entity.AddTexture(device.Load<Texture2D>("spritelib_bullet"));
        }

        public void BuildViewPortHeight(GraphicsDevice device)
        {
            entity.AddViewPortHeight(device.Viewport.Height); //Viewport height set from the games graphics viewport height
        }

        public void BuildViewPortWidth(GraphicsDevice device)
        {
            entity.AddViewPortWidth(device.Viewport.Width); //Viewport width set from the games graphics viewport width
        }

        public ConcreteEntity GetResult()
        {
            return entity;
        }

        public void Initialize(Game game)
        {
            entity.Initialize(game);
        }
    }
}
