﻿using CptS_487_game.Builder;
using CptS_487_game.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.EntityFactorys
{
    class EntityBuilderFactory
    {
        public IEntityBuilder CreateProduct(string product)
        {
            product = product.ToLower();
            switch (product)
            {
                case ("player"):
                    return new PlayerBuilder();
                case ("enemy"):
                    return new EnemyBuilder();
                case ("playerbullet"):
                    return new PlayerBulletBuilder();
                case ("enemybullet"):
                    return new EnemyBulletBuilder();
                case ("powerup"):
                    return new PowerupBuilder();
                default:
                    return null;
            }
        }
    }
}
