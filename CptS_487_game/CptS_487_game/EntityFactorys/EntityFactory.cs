﻿using CptS_487_game.Builder;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces.Entity;
using CptS_487_game.Interfaces;
using CptS_487_game.EntityDecorator;

namespace CptS_487_game.EntityFactorys
{
    class EntityFactory
    {
        Game _game;
        EntityDirector Director;
        EntityBuilderFactory BuilderFactory;
        IEntityBuilder builder;
        public EntityFactory(Game game)
        {
            _game = game;
            Director = new EntityDirector(_game);
            BuilderFactory = new EntityBuilderFactory();
        }

        public Entity CreateProduct(string type,string design, string movement, string bulletdesign, string bulletmovement,  Vector2 position, float start, float end)
        {
            type = type.ToLower();
            design = design.ToLower();
            movement = movement.ToLower();
            bulletdesign = bulletdesign.ToLower();
            bulletmovement = bulletmovement.ToLower();
            Entity entity = GetEntityType(position, type);
            entity.Start = start;
            entity.End = end;
            return GetEntityBulletDesign(
                    GetEntityBulletMovement(
                    GetEntityMovement(
                    GetEntityDesign(
                                entity,
                                design),
                                movement),
                                bulletmovement),
                                bulletdesign);
        }

        Entity GetEntityType(Vector2 position, string type)
        {
            switch (type)
            {
                case ("player"):
                    builder = BuilderFactory.CreateProduct("player");
                    break;
                    //Director.Construct(builder, position);
                    //return new DecoratorShooterPlayer(builder.GetResult());
                case ("enemy"):
                    builder = BuilderFactory.CreateProduct("enemy");
                    break;
                case ("powerup"):
                    builder = BuilderFactory.CreateProduct("powerup");
                    break;
                default:
                    return null;
            }
            Director.Construct(builder, position);
            return builder.GetResult();

        }

        Entity GetEntityDesign(Entity entity, string design)
        {
            switch (design)
            {
                case ("orangeplane"):
                    return new DecoratorDesignOrangePlane(entity);
                case ("player"):
                case ("greenplane"):
                    return new DecoratorDesignGreenPlane(entity);
                case ("ship2"):
                    return new DecoratorDesignShip2(entity);
                case ("blueplane"):
                    return new DecoratorDesignBluePlane(entity);
                case ("santamidboss"):
                    return new DecoratorDesignSantaMidBoss(entity);
                case ("santafinalboss"):
                    return new DecoratorDesignSantaFinalBoss(entity);
                case ("cake"):
                    return new DecoratorDesignCake(entity);
                case ("elf"):
                    return new DecoratorDesignElf(entity);
                case ("yellowball"):
                    return new DecoratorDesignYellowBall(entity);
                case ("bluegem"):
                    return new DecoratorDesignBlueGem(entity);
                case ("gardnerminchew"):
                case ("zak"):
                    return new DecoratorDesignZak(entity);
                default:
                    return entity;
            }
        }

        Entity GetEntityMovement(Entity entity, string movement)
        {
            switch (movement)
            {
                case ("controlled"):
                case ("player"):
                    return new DecoratorMovementControlled(entity);
                case ("downwave"):
                    return new DecoratorMovementDownWave(entity);
                case ("straight"):
                case ("downstraight"):
                    return new DecoratorMovementDownStraight(entity);
                case ("wave"):
                    return new DecoratorMovementDownWave(entity);
                case ("backforth"):
                    return new DecoratorMovementBackForth(entity);
                case ("downandoutleft"):
                    return new DecoratorMovementDownAndOutLeft(entity);
                case ("downandoutright"):
                    return new DecoratorMovementDownAndOutRight(entity);
                case ("right"):
                    return new DecoratorMovementRight(entity);
                case ("left"):
                    return new DecoratorMovementLeft(entity);
                case ("circleright"):
                case ("circle"):
                    return new DecoratorMovementCircleRight(entity);
                case ("circleleft"):
                    return new DecoratorMovementCircleLeft(entity);
                case ("stayput"):
                case ("none"):
                    return new DecoratorMovementStayPut(entity);
                default:
                    return entity;
            }
        }

        Entity GetEntityBulletDesign(Entity entity, string bulletdesign)
        {
            switch (bulletdesign)
            {
                case ("yellowball"):
                    return new DecoratorShooterYellowBall(entity);
                case ("yellowlaser"):
                    return new DecoratorShooterYellowLaser(entity);
                case ("blackbullet"):
                    return new DecoratorShooterBlackBullet(entity);
                case ("powerupdoubleshot"):
                    return new DecoratorPowerupDoubleShot(entity);
                case ("powerupblackbullet"):
                    return new DecoratorPowerupBlackBullet(entity);
                case ("poweruphealthpack"):
                    return new DecoratorPowerupHealthPack(entity);
                default:
                    return entity;
            }
        }

        Entity GetEntityBulletMovement(Entity entity, string bulletmovement)
        {
            switch (bulletmovement)
            {
                case ("upstraight"):
                    return new DecoratorShooterUpStraight(entity);
                case ("downstraight"):
                    return new DecoratorShooterDownStraight(entity);
                case ("downwave"):
                    return new DecoratorShooterDownWave(entity);
                case ("none"):
                case ("stayput"):
                    return new DecoratorShooterNone(entity);
                case ("secretfeature"):
                    return new DecoratorShooterSecretFeature(entity);
                default:
                    return entity;
            }
        }

    }
}
