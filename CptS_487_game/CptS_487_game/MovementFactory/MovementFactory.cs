﻿using CptS_487_game.Interfaces.Movement;
using CptS_487_game.MovementFactory.Movements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.MovementFactory
{
    class MovementCreator
    {
        public IMovable CreateProduct(string movement)
        {
            movement = movement.ToLower();

            switch (movement)
            {
                case ("player"):
                case ("controlled"):
                    return new MovementControlled();
                case ("playerbullet"):
                case ("upstraight"):
                    return new MovementUpStraight();
                case ("enemystraight"):
                case ("enemybullet"):
                case ("downstraight"):
                    return new MovementDownStraight();
                case ("enemywave"):
                case ("enemybulletwave"):
                case ("downwave"):
                    return new MovementDownWave();
                case ("stayput"):
                    return new MovementStayPut();
                case ("backforth"):
                    return new MovementBackForth();
                default:
                    return null;
            }
        }
    }
}
