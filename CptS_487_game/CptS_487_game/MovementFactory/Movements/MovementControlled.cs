﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces.Movement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using CptS_487_game.Control;

namespace CptS_487_game.MovementFactory.Movements
{
    class MovementControlled : IMovable
    {

        public Vector2 Move(Game game,Texture2D texture, Vector2 position, float speed)
        {
            float modifiedSpeed = speed;
            KeyboardState cks = Keyboard.GetState();
            if (cks.IsKeyDown(GameValues.GetInstance().Slowdown) || cks.IsKeyDown(Keys.RightShift)
                || GamePad.GetState(PlayerIndex.One).Buttons.RightShoulder == ButtonState.Pressed)
                modifiedSpeed *= .5f;

            if (cks.IsKeyDown(GameValues.GetInstance().Left)
                || GamePad.GetState(PlayerIndex.One).DPad.Left == ButtonState.Pressed)
                position.X -= modifiedSpeed;

            if (cks.IsKeyDown(GameValues.GetInstance().Right)
                || GamePad.GetState(PlayerIndex.One).DPad.Right == ButtonState.Pressed)
                position.X += modifiedSpeed;

            if (cks.IsKeyDown(GameValues.GetInstance().Up)
                || GamePad.GetState(PlayerIndex.One).DPad.Up == ButtonState.Pressed)
                position.Y -= modifiedSpeed;

            if (cks.IsKeyDown(GameValues.GetInstance().Down)
                || GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed)
                position.Y += modifiedSpeed;

            //Bound check
            position.X = MathHelper.Clamp(position.X, 0, game.GraphicsDevice.Viewport.Width - texture.Width);
            position.Y = MathHelper.Clamp(position.Y, 0, game.GraphicsDevice.Viewport.Height - texture.Height);
            return position;
    }
    }
}
