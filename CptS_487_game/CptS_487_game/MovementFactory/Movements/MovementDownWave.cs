﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces.Movement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace CptS_487_game.MovementFactory.Movements
{
    class MovementDownWave : IMovable
    {
        Random random;
        public Vector2 velocity;
        int randX;
        int decoratedSpeed;

        public MovementDownWave()
        {
            velocity = new Vector2(1, 0);
            decoratedSpeed = 2;
        }



        public Vector2 Move(Game game, Texture2D texture, Vector2 position, float speed)
        {
            // velocity.Y = decoratedSpeed;
            velocity.Y = decoratedSpeed;
            velocity.X = -(float)Math.Cos(-position.Y*0.05)*5;
            // Do not modify position
            // position += velocity;

            // Bounce horizontally
            //if (position.X <= 0 || position.X >= game.GraphicsDevice.Viewport.Width - texture.Width)
            //    velocity.X = -velocity.X;
            // Bounce vertically
            //if (Position.Y <= 0 || Position.Y >= _viewPortHeight - _texture.Height)
            //    velocity.Y = -velocity.Y;

            // Set invisible when out-of-bounds
            //if (position.X < 0 - texture.Width)
            //    isVisible = false;
            //if (Position.Y > _viewPortHeight + texture.Height)
            //    isVisible = false;

            // return position + velocity;

            Vector2 startPos = position;
            
            velocity = new Vector2(1, 0);

            position.Y += speed;
            position.X = startPos.X + ((float)Math.Cos(position.X / 100));

            return position;

        }
    }
}
