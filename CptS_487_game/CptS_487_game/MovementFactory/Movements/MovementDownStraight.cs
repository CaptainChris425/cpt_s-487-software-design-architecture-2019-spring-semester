﻿using CptS_487_game.Interfaces.Movement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.MovementFactory.Movements
{
    class MovementDownStraight : IMovable
    {
        public Vector2 velocity;
        public Vector2 Move(Game game, Texture2D texture, Vector2 position, float speed)
        {

            position.Y += speed;
             return position;

            /*Vector2 startPos = position;

            velocity = new Vector2(1, 0);

            position.Y += speed;
            position.X = startPos.X + ((float)Math.Cos(position.X / 100));

            return position;*/

        }
    }
}
