﻿using CptS_487_game.Interfaces.Shooting;
using CptS_487_game.MovementFactory.Shooting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.MovementFactory
{
    class ShooterFactory
    {
        public IShooter CreateProduct(string shooter)
        {
            shooter = shooter.ToLower();

            switch (shooter)
            {
                case ("player"):
                    return new ShooterTimedControlled();
                case ("enemy"):
                    return new ShooterTimed();
                case ("nottimes"):
                    return new ShooterNotTimed();
                case ("null"):
                    return new ShooterNull();
                default:
                    return null;
            }
        }
    }
}
