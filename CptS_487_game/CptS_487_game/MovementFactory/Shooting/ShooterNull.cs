﻿using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Shooting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.MovementFactory.Shooting
{
    class ShooterNull : IShooter
    {
        public IEntityBuilder Shoot(GameTime gt, KeyboardState cks, TimeSpan rateOfFire)
        {
            return null;
        }
    }
}
