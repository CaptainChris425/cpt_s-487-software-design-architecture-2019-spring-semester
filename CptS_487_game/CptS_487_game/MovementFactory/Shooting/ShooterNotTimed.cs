﻿using CptS_487_game.Builder;
using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Shooting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.MovementFactory.Shooting
{
    class ShooterNotTimed : IShooter
    {
        IEntityBuilder _bulletBuilder;

        public ShooterNotTimed()
        {
            _bulletBuilder = new EnemyBulletBuilder();
        }

        public IEntityBuilder Shoot(GameTime gt, KeyboardState cks, TimeSpan rateOfFire)
        {
            return _bulletBuilder;
        }
    }
}
