﻿using CptS_487_game.Builder;
using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Shooting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.MovementFactory.Shooting
{
    class EnemyWaveShooter : IShooter
    {
        TimeSpan _previousBulletSpawnTime;
        IEntityBuilder _bulletBuilder;

        public EnemyWaveShooter()
        {
            _bulletBuilder = new EnemyBulletBuilderWave();
        }

        public IEntityBuilder Shoot(GameTime gt, KeyboardState cks, TimeSpan rateOfFire)
        {
            //if (!cks.IsKeyDown(Keys.Z) && GamePad.GetState(PlayerIndex.One).Buttons.A != ButtonState.Pressed) { return null; }
            if (gt.TotalGameTime - _previousBulletSpawnTime > rateOfFire)
            {
                _previousBulletSpawnTime = gt.TotalGameTime;
                return _bulletBuilder;
            }

            return null;
        }
    }
}
