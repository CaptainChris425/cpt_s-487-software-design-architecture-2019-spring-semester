﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using CptS_487_game.Control;
using CptS_487_game.Menus;

namespace CptS_487_game
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        //Controllers for graphics, gameplay, sprites, and menu
        BulletHellController controller;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //All the menu stuff
        SpriteFont font;
        Menu menu;
        enum GameState { menu, game, gameOver, win};
        State curState; 

        // Window of the game attributes
        enum WindowScaling { OneToOne, PixelPerfect, AspectRatio, FullScreen };
        WindowScaling chosenWindowScaling;
        bool isCrispy = true;
        RenderTarget2D gameAreaTarget;

        // States to determine key presses
        KeyboardState currentKState;
        KeyboardState prevKState;

        // Song attributes
        bool paused = false;
        Song song;
        List<SoundEffect> soundEffects = new List<SoundEffect>();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // Use this for loading content from other classes
            Services.AddService(Content);

            // Create controller
            controller = new BulletHellController(this);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // Window settings
            chosenWindowScaling = WindowScaling.PixelPerfect;
            gameAreaTarget = new RenderTarget2D(GraphicsDevice, 240, 320);

            // Use this to draw textures
            GraphicsDevice.SetRenderTarget(gameAreaTarget); // most code uses viewport sizes
            spriteBatch = new SpriteBatch(GraphicsDevice);
            //graphics.ToggleFullScreen();

            Window.IsBorderless = true;
            Window.Position = new Point(0, 0);
            graphics.PreferMultiSampling = false;
            graphics.PreferredBackBufferWidth = 1920;
            graphics.PreferredBackBufferHeight = 1080;
            graphics.ApplyChanges();

            //Menu Initialization
            menu = new Menu(this);
            curState = new State();


            IsMouseVisible = true;
            controller.Initialize(spriteBatch);
            base.Initialize();
        }


        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Note: It's best to have hitboxes be separate from image size
            // Note: It's best to have the player clamping outside of the player itself

            //Loading menu font file (named file)
            font = Content.Load<SpriteFont>("File");
            menu.Initialize(font);

            song = Content.Load<Song>("onlymeith_-_Xmas_Time_II");
            //MediaPlayer.Play(song);
            //MediaPlayer.IsRepeating = true;

            soundEffects.Add(Content.Load<SoundEffect>("C_27P"));

            //player.fireSound = soundEffects[0].CreateInstance();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// 
        protected override void Update(GameTime gameTime)
        {
            //Checking where the player has clicked for the menu
            var mouseState = Mouse.GetState();
            var mousePoint = new Point(mouseState.X, mouseState.Y);
            var hover = new Rectangle(200, 265, 200, 35);
            //player.Update(gameTime);
            if (menu.Update(gameTime) == 1)
            {
                curState.gs = curState.setState("game");
            }
            if (curState.Update(gameTime))
            {
                if (hover.Contains(mousePoint) && mouseState.RightButton == ButtonState.Pressed)
                {
                    Exit();
                }
            }

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape)
                || menu.Update(gameTime) == 2)
                Exit();


            //End menu update here


            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            prevKState = currentKState;
            currentKState = Keyboard.GetState();

            // Display setting hotkeys
            int numWindowScalings = Enum.GetValues(typeof(WindowScaling)).Length;
            if (IsKeyPressed(Keys.F1, currentKState, prevKState))
                chosenWindowScaling = (WindowScaling)((int)(chosenWindowScaling + 1) % numWindowScalings);
            if (IsKeyPressed(Keys.F2, currentKState, prevKState))
                isCrispy = !isCrispy;
            if (IsKeyPressed(Keys.F3, currentKState, prevKState))
            {
                paused = !paused;
                if (paused)
                    MediaPlayer.Pause();
                else
                    MediaPlayer.Resume();
            }

            if (paused)
                return;

            GraphicsDevice.SetRenderTarget(gameAreaTarget); // most code uses viewport sizes

            if (curState.gs == State.GameState.game)
                //Spawn enemies and update player movement here
                controller.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        ///
        protected override void Draw(GameTime gameTime)
        {
            // Draw actual game things here
            GraphicsDevice.SetRenderTarget(gameAreaTarget);
            GraphicsDevice.Clear(Color.White);

            //This is for Drawing the menu
            var mouseState = Mouse.GetState();
            var mousePoint = new Point(mouseState.X, mouseState.Y);
            if (controller.gameOver)
            {
                curState.gs = State.GameState.menu;
                menu.menuState = Menu.GameState.gameOver;
            }
            // spriteBatch.DrawString(font, "" + mousePoint, new Vector2(200, 410), Color.Black);
            if (curState.gs != State.GameState.game)
            {
                




                spriteBatch.Begin();
                if (curState.gs == State.GameState.menu)
                {
                    GraphicsDevice.Clear(Color.Crimson);
                    menu.Draw(spriteBatch);
                }
                spriteBatch.End();
            }
            else
            {
                //End menu stuff here
                
                controller.Draw(gameTime);
            }
            


            // Go back to backbuffer (actual drawing screen) and draw the game scaled up
            {
                GraphicsDevice.SetRenderTarget(null);
                GraphicsDevice.Clear(Color.Black);

                // Do you want crispy pixels?
                spriteBatch.Begin(SpriteSortMode.Deferred, null, isCrispy ? SamplerState.PointClamp : null);
                double widthRatio = (double)Window.ClientBounds.Width / gameAreaTarget.Width;
                double heightRatio = (double)Window.ClientBounds.Height / gameAreaTarget.Height;
                double aspectRatio = Math.Min(widthRatio, heightRatio);
                var fullscreenRect = new Point(Window.ClientBounds.Width, Window.ClientBounds.Height);
                var aspectRatioRect = new Point((int)(gameAreaTarget.Width * aspectRatio), (int)(gameAreaTarget.Height * aspectRatio));
                var pixelPerfectRect = new Point((gameAreaTarget.Width * (int)aspectRatio), (gameAreaTarget.Height * (int)aspectRatio));
                var oneToOneRect = new Point(gameAreaTarget.Width, gameAreaTarget.Height);
                Point chosenRect = oneToOneRect;
                switch (chosenWindowScaling)
                {
                    case WindowScaling.OneToOne:
                        chosenRect = oneToOneRect;
                        break;
                    case WindowScaling.PixelPerfect:
                        chosenRect = pixelPerfectRect;
                        break;
                    case WindowScaling.AspectRatio:
                        chosenRect = aspectRatioRect;
                        break;
                    case WindowScaling.FullScreen:
                        chosenRect = fullscreenRect;
                        break;
                }
                spriteBatch.Draw(gameAreaTarget,
                    new Rectangle(CenterRectInRect(chosenRect, fullscreenRect), chosenRect), null, Color.White);
                spriteBatch.End();
            }

            // Gives list of supported resolutions
            //var displayModes = GraphicsAdapter.DefaultAdapter.SupportedDisplayModes;

            base.Draw(gameTime);
        }

        // Helper functions
        private Point CenterRectInRect(Point a, Point b)
        {
            Point c = a;
            c.X = (b.X - a.X) / 2;
            c.Y = (b.Y - a.Y) / 2;
            return c;
        }

        private bool IsKeyPressed(Keys key, KeyboardState curr, KeyboardState prev)
        {
            return curr.IsKeyDown(key) && prev.IsKeyUp(key);
        }
    }
}
