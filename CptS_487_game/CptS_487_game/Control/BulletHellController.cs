﻿using CptS_487_game.Builder;
using CptS_487_game.EntityDecorator;
using CptS_487_game.EntityFactorys;
using CptS_487_game.Interfaces;
using CptS_487_game.MovementFactory.Movements;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Control;

namespace CptS_487_game.Control
{
    class BulletHellController
    {
        Game _game;
        SpriteBatch _sb;
        public bool gameOver;
        public Entity Player;
        List<Entity> PlayerBullets;
        List<Entity> Enemies;
        bool parsed;

        //Integrate multiple controllers
        MovementController movementController;
        SpawningController spawningController;
        ShootingController shootingController;
        DrawingController drawingController;
        CollisionController collisionController;

        public BulletHellController(Game game)
        {
            _game = game;
            gameOver = false;
            //Lists
            Player = new ConcreteEntity();
            PlayerBullets = new List<Entity>();
            Enemies = new List<Entity>();
            // Controllers
            movementController = new MovementController(game);
            shootingController = new ShootingController(game);
            spawningController = new SpawningController(game);
            drawingController = new DrawingController(game);
            collisionController = new CollisionController(game);

            //Testing spawning controller json parsing
            
            //
            parsed = false;

            
        }

        public void Initialize(SpriteBatch sb)
        {
            _sb = sb;
        }

        public void Update(GameTime gameTime)
        {
            if (!parsed)
            {
                var level = GameValues.GetInstance().Level;
                spawningController.LevelParser(level);
                parsed = true;
            }
            spawningController.UpdateEntities(gameTime, ref Player,ref Enemies, ref PlayerBullets);
            movementController.MoveAll(Player, PlayerBullets, Enemies);
            shootingController.ShootAll(Player, PlayerBullets, Enemies, gameTime);         
            collisionController.CheckCollisions(ref Player, ref PlayerBullets, ref Enemies);
            if (Player.Health <= 0) gameOver = true;
        }

        public void Draw(GameTime gameTime)
        {
            _sb.Begin();
            drawingController.DrawAll(Player, PlayerBullets, Enemies, _sb);
            _sb.End();
        }
    }
}
