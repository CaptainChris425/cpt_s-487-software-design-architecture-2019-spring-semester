﻿using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Control
{
    class DrawingController
    {
        Game game;
        Texture2D stars;
        //Empty constructor
        public DrawingController(Game g)
        {
            game = g;
            stars = null;
        }

        public void DrawAll(Entity player, List<Entity> playerBullets, List<Entity> enemies, SpriteBatch sb)
        {
            if (stars == null) stars = game.Content.Load<Texture2D>("LQk8v");
            player.Draw(sb);
            DrawLives(player, sb);
            foreach (var e in playerBullets)
                e.Draw(sb);

            foreach (var e in enemies)
                e.Draw(sb);
        }

        public void DrawLives(Entity player, SpriteBatch sb)
        {
            for (int i = 0; i < player.Health; i++)
            {
                sb.Draw(stars, new Vector2(200 + (i * 10), 300), null, Color.White, 0f, Vector2.Zero, 0.012f, SpriteEffects.None, 0f);
            }
        }

    }
}
