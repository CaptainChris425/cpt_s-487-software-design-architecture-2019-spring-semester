﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;


namespace CptS_487_game.Control
{
    public class GameValues
    {
        private static readonly GameValues instance = new GameValues();
        public Keys Up
        {
            get; set;
        }
        public Keys Down
        {
            get; set;
        }
        public Keys Left
        {
            get; set;
        }
        public Keys Right
        {
            get; set;
        }
        public Keys Shoot
        {
            get; set;
        }
        public Keys Slowdown
        {
            get; set;
        }
        public string Level
        {
            get; set;
        }
        private float dm;
        public float Difficulty
        {
            get
            {
                return dm > 0 ? dm / 6 : (float)0.1;
            }
        }
        public float DifficultyMultiplier
        {
            get => dm; 
            set => dm = value; 
        }
        private GameValues()
        {
            Up = Keys.Up;
            Down = Keys.Down;
            Left = Keys.Left;
            Right = Keys.Right;
            Shoot = Keys.Z;
            Slowdown = Keys.LeftShift;
            DifficultyMultiplier = 6;
            Level = "NormalLevel.json";
        }
        
        public static GameValues GetInstance()
        { 
            return instance;
        }
    }
}
