﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Control.Misc
{
    class PositionHolder
    {
        public float x { get; set; }
        public float y { get; set; }
        public bool spawned { get; set; }
        public Vector2 position { get => new Vector2(x, y); }
    }

    class TimeHolder
    {
        public float start { get; set; }
        public float end { get; set; }
    }

    class WaveHolder
    {
        public TimeHolder time { get; set; }
        public List<EntityHolder> entities { get; set; }
        public List<EntityHolder> powerups { get; set; }
    }

    class EntityHolder
    {
        public string type { get; set; }
        public string design { get; set; }
        public string movement { get; set; }
        public string bulletdesign { get; set; }
        public string bulletmovement { get; set; }
        public string powerup { get; set; }
        public int quantity { get; set; }
        public List<PositionHolder> positions { get; set; }
        public List<PositionHolder> positionsrelative { get; set; }
        public TimeHolder time { get; set; }
    }

    class LevelHolder
    {
        public EntityHolder player { get; set; }
        public List<WaveHolder> waves {get;set;}
    }
}
