﻿using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Control
{
    //Separate movement from the main controller
    //Let it interact with this like a Facade
    class MovementController
    {
        Game game;

        //Empty constructor
        public MovementController(Game g)
        {
            game = g;
        }

        public void MoveAll(Entity player, List<Entity> playerBullets, List<Entity> enemies)
        {
            player.Move();
            MovePlayerBullets(playerBullets);
            MoveEnemies(enemies);
        }

        private void MovePlayerBullets(List<Entity> playerBullets)
        {
            foreach (var e in playerBullets)
            {
                e.Move();
                if (e.Position.Y < 0)
                    e.Active = false;
            }
        }

        private void MoveEnemies(List<Entity> enemies)
        {
            foreach (var e in enemies)
            {
                e.Move();
                if (e.Position.Y > game.GraphicsDevice.Viewport.Height + 50)
                    e.Active = false;
            }
        }
    }
}
