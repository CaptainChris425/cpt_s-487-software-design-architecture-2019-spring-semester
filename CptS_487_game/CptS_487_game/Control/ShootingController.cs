﻿using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Control
{
    //Separate movement from the main controller
    //Let it interact with this like a Facade
    class ShootingController
    {
        Game game;

        //Empty constructor
        public ShootingController(Game g)
        {
            game = g;
        }

        public void ShootAll(Entity player, List<Entity> playerBullets, List<Entity> enemies, GameTime gameTime)
        {
            GetBullet(player, playerBullets, gameTime);

            for (var i = enemies.Count - 1; i >= 0; --i) // So we can add to Enemies during loop
                GetBullet(enemies[i], enemies, gameTime);
        }

        private void GetBullet(Entity entity, List<Entity> bulletList, GameTime gameTime)
        {
            List<Entity> bullet = entity.Shoot(gameTime, Keyboard.GetState());

            if (bullet != null)
            {
                bulletList.AddRange(bullet);
            }
        }

    }
}
