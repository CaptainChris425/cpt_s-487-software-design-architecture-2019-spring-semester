﻿using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CptS_487_game.Control
{
    class CollisionController
    {
        Game game;

        //Empty constructor
        public CollisionController(Game g)
        {
            game = g;
        }

        public void CheckCollisions(ref Entity player, ref List<Entity> playerBullets, ref List<Entity> enemies)
        {

            for (int i = 0; i < enemies.Count; i++)
            {
                if (enemies[i].DamagesEnemies == true)
                {
                    for (int j = 0; j < enemies.Count; j++)
                    {
                        if (i != j)
                        {
                            if (enemies[i].HitBox.Intersects(enemies[j].HitBox))
                            {
                                enemies[i].ApplyEffect(enemies[j]);
                            }
                        }
                    }
                }
            }

            var cks = Keyboard.GetState();
            if (!cks.IsKeyDown(Keys.LeftControl))
            {
                // Kill the player when colliding with DemonEnemy
                for (int i = 0; i < enemies.Count; i++)
                {
                    if (player.HitBox.Intersects(enemies[i].HitBox))
                    {
                        player = enemies[i].ApplyEffect(player);
                        Respawn(player);
                    }
                }
            }
            // Check if fireball intersects with demon enemy
            for (int i = 0; i < playerBullets.Count; i++)
            {
                for (int x = 0; x < enemies.Count; x++)
                {

                    if (playerBullets[i].HitBox.Intersects(enemies[x].HitBox))
                    {
                        enemies[x] = playerBullets[i].ApplyEffect(enemies[x]);
                        // Remove fireball from list
                        playerBullets[i].Active = false;
                        break;
                    }
                }
            }
        }


        void Respawn(Entity player)
        {
            player.Position = new Vector2(game.GraphicsDevice.Viewport.TitleSafeArea.X + game.GraphicsDevice.Viewport.TitleSafeArea.Width / 2,
                      game.GraphicsDevice.Viewport.TitleSafeArea.Y + 3 * game.GraphicsDevice.Viewport.TitleSafeArea.Height / 2);
        }




    }
}
