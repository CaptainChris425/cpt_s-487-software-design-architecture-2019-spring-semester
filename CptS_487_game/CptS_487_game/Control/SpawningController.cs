﻿using CptS_487_game.EntityFactorys;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using CptS_487_game.Control.Misc;
using System.IO;
using System;

namespace CptS_487_game.Control
{
    class SpawningController
    {
        Game _game;
        EntityFactory entityFactory;
        float startSeconds;
        LevelHolder level;
        int maxX, maxY;

        class TypeOfEntity
        {
            Entity entity;
            float start;
            float end;
        }

        public SpawningController(Game game)
        {
            _game = game;
            entityFactory = new EntityFactory(game);
            level = new LevelHolder();
            

        }

        public void LevelParser(string jsonFile)
        {
            var x = Environment.CurrentDirectory;
            using (StreamReader r = new StreamReader(jsonFile))
            {
                string jsonObj = r.ReadToEnd();
                level = new JavaScriptSerializer().Deserialize<LevelHolder>(jsonObj);
            }
            
        }

        public void UpdateEntities(GameTime gt, ref Entity Player, ref List<Entity> Enemies, ref List<Entity> PlayerBullets)
        {
            SpawnEntities(gt, ref Player, ref Enemies);
            DespawnEntities(gt, ref Enemies, ref PlayerBullets);
        }

        public void SpawnEntities(GameTime gt, ref Entity Player, ref List<Entity> Enemies)
        {
            maxX = _game.GraphicsDevice.Viewport.Width - 30;
            maxY = _game.GraphicsDevice.Viewport.Height;
            var elapsedSeconds = gt.TotalGameTime.Seconds;
            if (startSeconds == default(float))
            {
                //If the start seconds havent been assigned
                //then assign startseconds and create the player
                startSeconds = gt.TotalGameTime.Seconds;
                Player = SpawnPlayer();
            }
            foreach (var wave in level.waves)
            {
                if (wave.time.start < (elapsedSeconds - startSeconds))
                {
                    foreach (var entity in wave.entities)
                    {
                        if (entity.time.start < (elapsedSeconds - startSeconds))
                        {
                            if (entity.positions != null)
                            {
                                foreach (var position in entity.positions)
                                {
                                    if (!position.spawned)
                                    {
                                        Enemies.Add(SpawnEnemy(entity, position.position));
                                        position.spawned = true;
                                    }
                                }
                            }
                            if (entity.positionsrelative != null)
                            {
                                foreach (var position in entity.positionsrelative)
                                {
                                    if (!position.spawned)
                                    {
                                        Vector2 relativeposition = new Vector2(position.x * maxX, position.y * maxY);
                                        Enemies.Add(SpawnEnemy(entity, relativeposition));
                                        position.spawned = true;
                                    }
                                }
                            }
                        }

                    }
                    foreach (var entity in wave.powerups)
                    {
                        if (entity.time.start < (elapsedSeconds - startSeconds))
                        {
                            if (entity.positions != null)
                            {
                                foreach (var position in entity.positions)
                                {
                                    if (!position.spawned)
                                    {
                                        Enemies.Add(SpawnPowerup(entity, position.position));
                                        position.spawned = true;
                                    }
                                }
                            }
                            if (entity.positionsrelative != null)
                            {
                                foreach (var position in entity.positionsrelative)
                                {
                                    if (!position.spawned)
                                    {
                                        Vector2 relativeposition = new Vector2(position.x * maxX, position.y * maxY);
                                        Enemies.Add(SpawnPowerup(entity, relativeposition));
                                        position.spawned = true;
                                    }
                                }
                            }
                        }

                    }

                    

                }
            } 
        }

        public void DespawnEntities(GameTime gt, ref List<Entity> Enemies, ref List<Entity> PlayerBullets)
        {
            var elapsedSeconds = gt.TotalGameTime.Seconds;
            var time = elapsedSeconds - startSeconds;
            for (var i = 0; i < Enemies.Count; i++)
            {
                if (time > Enemies[i].End ||
                    !Enemies[i].Active ||
                    Enemies[i].Position.X > _game.GraphicsDevice.Viewport.Width + 10 ||
                    Enemies[i].Position.X < -10 ||
                    Enemies[i].Position.Y > _game.GraphicsDevice.Viewport.Height + 10 ||
                    Enemies[i].Position.Y < -10 ||
                    Enemies[i].Health < 0
                    )
                {
                    Enemies.RemoveAt(i);
                    i--;
                }
            }
            for (var i = 0; i < PlayerBullets.Count; i++)
            {
                if (time > PlayerBullets[i].End ||
                    !PlayerBullets[i].Active ||
                    PlayerBullets[i].Position.X > _game.GraphicsDevice.Viewport.Width - PlayerBullets[i].Width ||
                    PlayerBullets[i].Position.X < -10 ||
                    PlayerBullets[i].Position.Y > _game.GraphicsDevice.Viewport.Height - PlayerBullets[i].Height ||
                    PlayerBullets[i].Position.Y < -10
                    )
                {
                    PlayerBullets.RemoveAt(i);
                    i--;
                }
            }

        }

        Entity SpawnPlayer()
        {
            //Vector2 playerPosition = level.player.position.position;
            return entityFactory.CreateProduct(
                level.player.type,
                level.player.design,
                level.player.movement,
                level.player.bulletdesign,
                level.player.bulletmovement,
                level.player.positions[0].position,
                level.player.time.start,
                level.player.time.end);
        }

        Entity SpawnEnemy(EntityHolder e, Vector2 position)
        {
            return entityFactory.CreateProduct(
                e.type,
                e.design,
                e.movement,
                e.bulletdesign,
                e.bulletmovement,
                position,
                e.time.start,
                e.time.end);
        }

        Entity SpawnPowerup(EntityHolder e, Vector2 position)
        {
            return entityFactory.CreateProduct(
                e.type,
                e.design,
                e.movement,
                e.powerup,
                "none",
                position, 
                e.time.start,
                e.time.end);
        }
    }
}
