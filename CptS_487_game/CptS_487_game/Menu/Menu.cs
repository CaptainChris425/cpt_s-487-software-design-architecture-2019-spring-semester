﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using CptS_487_game.Control;

namespace CptS_487_game.Menus
{
    class Menu
    {
        private SpriteFont font;
        private bool settingKey;
        int maxX, maxY;
        public enum GameState { menu,game, replay, player, music, opts , quit, gameOver, restart, levelSelection};
        public GameState menuState = GameState.menu;
        private Game _game;
        private Color[] fontColors = new Color[7];
        private Rectangle[] menuOpts = new Rectangle[7];
        string[] menuItems = { "Game Start", "Extra Start", "Replay", "Level Select", "Music", "Options", "Quit" };
        string[] optionsItems = { "Move up", "Move down", "Move left", "Move right", "Shoot", "Difficulty", "Menu" };
        string[] leveloptions = { "Hard", "Normal", "Easy", "Menu"};

        private double lastMouseClick = 0, timeBetweenClicks = 100;
        
        public Menu(Game game)
        {
            _game = game;
        }
        public void Initialize(SpriteFont sf)
        {
            settingKey = false;
            maxX = _game.GraphicsDevice.Viewport.Width;
            maxY = _game.GraphicsDevice.Viewport.Height;
            font = sf;
            for (int i = 0; i < 7; i++)
            {
                menuOpts[i] = new Rectangle(maxX/3, 375 + 90 * i, 1200, 70);
                //each rectangle represents a menu item, as coded in Draw()
                fontColors[i] = Color.Black;
            }
        }

        public int Update(GameTime gt)
        {
            if (settingKey) return 0;
            var mouseState = Mouse.GetState();
            var mousePoint = new Point(mouseState.X, mouseState.Y);
            //check if any menu options have been clicked on via mouse
            //menu items change color when hovered
            for (int i = 0; i < menuOpts.Length; i++)
            {
                // playArea = new Rectangle(100, 100 + 50 * i, 200, 35);
                if (menuOpts[i].Contains(mousePoint))
                {
                    fontColors[i] = Color.Blue;
                    
                }
                else
                { fontColors[i] = Color.Black; }
            }
            if (menuState == GameState.menu)
            {
                GetMenuSelection(mouseState, mousePoint, gt);
            }
            else if (menuState == GameState.opts)
            {
                GetOptionsSelection(mouseState, mousePoint, gt);
            }
            else if (menuState == GameState.gameOver)
            {
                GetGameOverSelection(mouseState, mousePoint, gt);
            }
            else if (menuState == GameState.levelSelection)
            {
                GetLevelSelection(mouseState, mousePoint, gt);
            }
            else if (menuState == GameState.game)
            {
                return 1;
            }
            else if (menuState == GameState.quit)
            {
                return 2;
            }
             return 0;
        }

        void GetGameOverSelection(MouseState mouseState, Point mousePoint, GameTime gt)
        {
            if (mouseState.RightButton == ButtonState.Pressed || mouseState.LeftButton == ButtonState.Pressed)
            {
                if (lastMouseClick == 0) lastMouseClick = gt.TotalGameTime.TotalMilliseconds;
                else if (gt.TotalGameTime.TotalMilliseconds - lastMouseClick < timeBetweenClicks) return;
                if (menuOpts[2].Contains(mousePoint))
                {
                    menuState = GameState.quit;
                }
            }
        }

        void GetLevelSelection(MouseState mouseState, Point mousePoint, GameTime gt)
        {
            if (mouseState.RightButton == ButtonState.Pressed || mouseState.LeftButton == ButtonState.Pressed)
            {
                if (lastMouseClick == 0) lastMouseClick = gt.TotalGameTime.TotalMilliseconds;
                else if (gt.TotalGameTime.TotalMilliseconds - lastMouseClick < timeBetweenClicks) return;
                if (menuOpts[0].Contains(mousePoint))
                {
                    //set up
                    GameValues.GetInstance().Level = "HardLevel.json";
                }
                else if (menuOpts[1].Contains(mousePoint))
                {
                    //set down
                    GameValues.GetInstance().Level = "NormalLevel.json";
                }
                else if (menuOpts[2].Contains(mousePoint))
                {
                    //set left
                    GameValues.GetInstance().Level = "EasyLevel.json";
                }
                else if (menuOpts[4].Contains(mousePoint))
                {
                    //Return to menu
                    menuState = GameState.menu;
                }
                lastMouseClick = gt.TotalGameTime.TotalMilliseconds;
            }
        }

        void GetMenuSelection(MouseState mouseState, Point mousePoint, GameTime gt)
        {
            if (mouseState.RightButton == ButtonState.Pressed || mouseState.LeftButton == ButtonState.Pressed)
            {
                if (lastMouseClick == 0) lastMouseClick = gt.TotalGameTime.TotalMilliseconds;
                else if (gt.TotalGameTime.TotalMilliseconds - lastMouseClick < timeBetweenClicks) return;
                if (menuOpts[0].Contains(mousePoint))
                {
                    menuState = GameState.game;
                }//play game
                else if (menuOpts[3].Contains(mousePoint)){
                    menuState = GameState.levelSelection;
                }
                else if (menuOpts[5].Contains(mousePoint))
                { menuState = GameState.opts; }//go to options
                else if (menuOpts[6].Contains(mousePoint))
                { menuState = GameState.quit; }//exit game
                lastMouseClick = gt.TotalGameTime.TotalMilliseconds;
            }
        }

        void GetOptionsSelection(MouseState mouseState, Point mousePoint, GameTime gt)
        {
            var cks = Keyboard.GetState();
            if (mouseState.RightButton == ButtonState.Pressed || mouseState.LeftButton == ButtonState.Pressed)
            {
                if (lastMouseClick == 0) lastMouseClick = gt.TotalGameTime.TotalMilliseconds;
                else if (gt.TotalGameTime.TotalMilliseconds - lastMouseClick < timeBetweenClicks) return;
                if (menuOpts[0].Contains(mousePoint))
                {
                    //set up
                    SetGameValue("up", gt);
                }
                else if (menuOpts[1].Contains(mousePoint))
                {
                    //set down
                    SetGameValue("down", gt);
                }
                else if (menuOpts[2].Contains(mousePoint))
                {
                    //set left
                    SetGameValue("left", gt);
                }
                else if (menuOpts[3].Contains(mousePoint))
                {
                    //set right
                    SetGameValue("right", gt);
                }
                else if (menuOpts[4].Contains(mousePoint))
                {
                    //Set shoot
                    SetGameValue("shoot", gt);
                }
                else if (menuOpts[5].Contains(mousePoint))
                {
                    //Set difficulty
                    SetGameValue("difficulty", gt);
                }
                else if (menuOpts[6].Contains(mousePoint))
                {
                    //Return to menu
                    menuState = GameState.menu;
                }
                lastMouseClick = gt.TotalGameTime.TotalMilliseconds;
            }
        }

        void SetGameValue(string option, GameTime gt)
        {
            
            switch (option)
            {
                case ("up"):
                    GameValues.GetInstance().Up = GetPressedKey(gt);
                    break;
                case ("down"):
                    GameValues.GetInstance().Down = GetPressedKey(gt);
                    break;
                case ("left"):
                    GameValues.GetInstance().Left = GetPressedKey(gt);
                    break;
                case ("right"):
                    GameValues.GetInstance().Right = GetPressedKey(gt);
                    break;
                case ("shoot"):
                    GameValues.GetInstance().Shoot = GetPressedKey(gt);
                    break;
                case ("difficulty"):
                    GameValues.GetInstance().DifficultyMultiplier = (GameValues.GetInstance().DifficultyMultiplier + 2) % 10;
                    break;
            }
        }

        Keys GetPressedKey(GameTime gt)
        {
            var cks = Keyboard.GetState();
            foreach (Keys key in (Keys[])Enum.GetValues(typeof(Keys)))
            {
                if (cks.IsKeyDown(key))
                {
                    return key;
                }
            }
            var rand = new Random(gt.TotalGameTime.Milliseconds);
            return Keys.A + rand.Next(0, 25);
        }
        public void Draw(SpriteBatch sb)
        {
            sb.DrawString(font, "Faith", new Vector2(85, 10), Color.Wheat);
            sb.DrawString(font, "Mountain", new Vector2(50, 40), Color.Wheat);

            if (menuState == GameState.menu)
            {
                PrintMenu(sb);
            }
            else if (menuState == GameState.game)
            {
                
            }
            else if(menuState == GameState.opts)
            {
                PrintOptions(sb);
            }
            else if(menuState == GameState.gameOver)
            {
                PrintGameOver(sb);
            }
            else if (menuState == GameState.levelSelection)
            {
                PrintLevelSelection(sb);
            }
            
        }

        void PrintMenu(SpriteBatch sb)
        {
            sb.DrawString(font, "Menu", new Vector2(0, 75), Color.Black);
            for (int i = 0; i < menuItems.Length; i++)
            {
                sb.DrawString(font, menuItems[i], new Vector2(15, 100 + 30 * i), fontColors[i]);
            }
        }
        void PrintOptions(SpriteBatch sb)
        {
            string[] values = { " ["+GameValues.GetInstance().Up.ToString()+"]",
                                      " ["+GameValues.GetInstance().Down.ToString()+"]",
                                      " ["+GameValues.GetInstance().Left.ToString()+"]",
                                      " ["+GameValues.GetInstance().Right.ToString()+"]",
                                      " ["+GameValues.GetInstance().Shoot.ToString()+"]",
                                      (" ["+GameValues.GetInstance().DifficultyMultiplier).ToString()+"]", "" };
            sb.DrawString(font, "Options", new Vector2(0, 75), Color.Black);
            for (int i = 0; i < optionsItems.Length; i++)
            {
                sb.DrawString(font, optionsItems[i] + values[i], new Vector2(15, 100 + 30 * i), fontColors[i]);
            }
        }

        void PrintGameOver(SpriteBatch sb)
        {
            sb.DrawString(font, "Game Over!", new Vector2(0,75), Color.Black);
            sb.DrawString(font, "End Game", new Vector2(15, 160), fontColors[2]);
        }

        void PrintLevelSelection(SpriteBatch sb)
        {
            sb.DrawString(font, "Level Selection", new Vector2(0, 75), Color.Black);
            string selection = " []";
            if(GameValues.GetInstance().Level == "HardLevel.json")
            {
                selection = " [X]";
            }
            sb.DrawString(font, leveloptions[0] + selection, new Vector2(15, 100 + 30 * 0), fontColors[0]);
            selection = " []";
            if (GameValues.GetInstance().Level == "NormalLevel.json")
            {
                selection = " [X]";
            }
            sb.DrawString(font, leveloptions[1] + selection, new Vector2(15, 100 + 30 * 1), fontColors[1]);
            selection = " []";
            if (GameValues.GetInstance().Level == "EasyLevel.json")
            {
                selection = " [X]";
            }
            sb.DrawString(font, leveloptions[2] + selection, new Vector2(15, 100 + 30 * 2), fontColors[2]);
            sb.DrawString(font, leveloptions[3], new Vector2(15, 100 + 30 * 4), fontColors[4]);
        }
    }
}
