﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;


namespace CptS_487_game.Menus
{
    class State
    {

       public enum GameState { menu, game, gameOver, win, quit };
       public GameState gs = GameState.menu;
       private SpriteFont f;
       public void Initialize(SpriteFont font)
        {
            f = font;
        }
       public GameState setState(String s)
        {
            switch (s)
            {
                case "menu":
                    return GameState.menu;                    
                case "game":
                    return GameState.game;
                case "gameOver":
                    return GameState.gameOver;
                case "win":
                    return GameState.win;
                case "quit":
                    return GameState.quit;
                default:
                    return GameState.game;

            }
        }
        public bool Update(GameTime gt )
        {
            var mouseState = Mouse.GetState();
            var mousePoint = new Point(mouseState.X, mouseState.Y);
            var hover = new Rectangle(200, 265, 200, 35);

            if (gs == State.GameState.win || gs == State.GameState.gameOver)
            {
                if (hover.Contains(mousePoint) && mouseState.RightButton == ButtonState.Pressed)
                {
                    return true;
                }
            }
            return false;
        }
        //public bool isState(string s)
        //{
        //    return gs;
        //}
       //public void Draw(SpriteBatch sb, GraphicsDevice gd)
       // {
       //     var mouseState = Mouse.GetState();
       //     var mousePoint = new Point(mouseState.X, mouseState.Y);

       //     if (gs == GameState.gameOver)
       //     {
       //         sb.DrawString(f, "Game Over!", new Vector2(200, 210), Color.Black);
       //     }
       //     if (gs == GameState.win)
       //     {
       //         sb.DrawString(f, "YOU WIN!", new Vector2(200, 210), Color.Black);
       //     }
       //     if (gs == GameState.win || gs == GameState.gameOver)
       //     {
       //         var fColor = new Color();
       //         fColor = Color.Black;
       //         var hover = new Rectangle(200, 265, 200, 35);

       //         if (hover.Contains(mousePoint))
       //         {
       //             fColor = Color.Red;
       //         }

       //         sb.DrawString(f, "End Game", new Vector2(200, 265), fColor);
       //     }
       //     if (gs == GameState.menu)
       //     {
       //        gd.Clear(Color.Crimson);
       //         menu.Draw(sb);
       //     }
       //     //draw health
       //     //spriteBatch.DrawString(f, "Lives: " + player.Lives, new Vector2(200, 410), Color.Black);
       //     if (gs == GameState.game)
       //     {
       //         //Draw player
       //         player.Draw(spriteBatch);
       //         //draw player lives with # of stars
       //         player.drawLives(spriteBatch);
       //         foreach (Enemy enemy in enemies)
       //         {
       //             enemy.Draw(spriteBatch);
       //         }
       //     }
       // }
    }
}
