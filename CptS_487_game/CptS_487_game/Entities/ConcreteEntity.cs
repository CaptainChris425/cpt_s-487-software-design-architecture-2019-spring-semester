﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Entity;
using CptS_487_game.Interfaces.Movement;
using Microsoft.Xna.Framework.Audio;
using CptS_487_game.Builder;
using CptS_487_game.Interfaces.Shooting;

namespace CptS_487_game.Objects
{
    class ConcreteEntity : Entity
    {
        public new SoundEffectInstance fireSound;
        protected float _speed;
        protected float _damage;
        protected Texture2D _texture;
        protected Vector2 _position;
        protected bool _active;
        protected float _health; //Not yet implemented
        protected int _viewPortWidth;
        protected int _viewPortHeight;
        protected int _width;
        protected int _height;
        protected IMovable _movable;
        protected TimeSpan _rateOfBullets;
        protected TimeSpan _previousBulletSpawnTime;
        protected IShooter _shooter;
        protected EntityDirector _entityDirector;
        protected Game game;
        protected float _start;
        protected float _end;
        protected Vector2 _bulletAdjustment;
        protected bool _damagesEnemies;
        protected Rectangle _hitBox;


        // Getters/Setters
        public override bool Active { get => _active; set => _active = value; }
        public override float Speed { get => _speed; set => _speed = value; }
        public override int Width { get => Texture.Width; }
        public override int Height { get => Texture.Height; }
        public override Vector2 Position { get => _position;  set => _position = value; }
        public override Game Game { get => game; }
        public override SoundEffectInstance FireSound { get => fireSound; }
        public override TimeSpan RateOfBullets { get => _rateOfBullets; set => _rateOfBullets = value; }
        public override Texture2D Texture {get => _texture; set => _texture = value;}
        public override IShooter Shooter { get => _shooter; set => _shooter = value; }
        public override IMovable Movable { get => _movable; set => _movable = value; }
        public override EntityDirector EntityDirector { get => _entityDirector; set => _entityDirector = value; }
        public override float Health { get => _health; set => _health = value; }
        public override float Damage { get => _damage; set => _damage = value; }
        public override int ViewPortHeight { get => _viewPortHeight; set => _viewPortHeight = value; }
        public override int ViewPortWidth { get => _viewPortWidth; set => _viewPortWidth = value; }
        public override float Start { get => _start; set => _start = value; }
        public override float End { get => _end; set => _end = value; }
        public override Vector2 BulletAdjustment { get => _bulletAdjustment; set => _bulletAdjustment = value; }
        public override bool DamagesEnemies { get => _damagesEnemies; set => _damagesEnemies = value; }
        public override Rectangle HitBox { get => new Rectangle((int)Position.X, (int)Position.Y, Width, Height);
                                            set => _hitBox = value; }
        //Abstract functions

        //Virtual functions
        public override void Initialize(Game game)
        {
            this.game = game;
            _previousBulletSpawnTime = new TimeSpan(0, 0, 0);
            _bulletAdjustment = new Vector2(0, 0);
        }
 
        public override void Draw(SpriteBatch sb)
        {
            sb.Draw(_texture, _position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }

        public override void Move()
        {
            _position = _movable.Move(game,_texture,_position, _speed);
            
        }

        public override List<Entity> Shoot(GameTime gt, KeyboardState cks){
            List<Entity> shot = new List<Entity>();
            IEntityBuilder bullet = _shooter.Shoot(gt, cks, _rateOfBullets);
            if (bullet != null)
            {
                // + new Vector2(8,-19) for player
                EntityDirector.Construct(bullet, Position + BulletAdjustment);
                Entity b = bullet.GetResult();
                b.End = 9999;
                shot.Add(b);
                return shot;
                //fireSound.Play();
            }
            
            //if (!cks.IsKeyDown(Keys.Z) && GamePad.GetState(PlayerIndex.One).Buttons.A != ButtonState.Pressed) { return null; }
            //if (gt.TotalGameTime - _previousBulletSpawnTime > _rateOfBullets)
            //{
            //    _previousBulletSpawnTime = gt.TotalGameTime;
                
            //    fireSound.Play();
            //    return _bulletBuilder;
            //}
            return null;
        }

        public override void TakeDamage(float d)
        {
            _health -= d;
            if (_health <= 0) _active = false;
        }

        public override Entity ApplyEffect(Entity e)
        {
            e.Health -= Damage;
            return e;
        }
    }
}
