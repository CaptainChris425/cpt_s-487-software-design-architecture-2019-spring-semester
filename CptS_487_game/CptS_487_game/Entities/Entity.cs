﻿using CptS_487_game.Builder;
using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Entity;
using CptS_487_game.Interfaces.Movement;
using CptS_487_game.Interfaces.Shooting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Objects
{
    abstract class Entity : Object,IEntityBuildable, IEntityDrawable, IEntityMoveable, IEntityShootable, IEntityDamagable
    {
        virtual public bool Active { get; set; }
        virtual public float Speed { get; set; }
        virtual public int Width { get; set; }
        virtual public int Height { get; set; }
        virtual public Vector2 Position { get; set; }
        virtual public Game Game { get; set; }
        virtual public SoundEffectInstance FireSound { get; set; }
        virtual public TimeSpan RateOfBullets { get; set; }
        virtual public Texture2D Texture { get; set; }
        virtual public IShooter Shooter { get; set; }
        virtual public IMovable Movable { get; set; }
        virtual public EntityDirector EntityDirector { get; set; }
        virtual public float Health { get; set; }
        virtual public float Damage { get; set; }
        virtual public int ViewPortHeight { get; set; }
        virtual public int ViewPortWidth { get; set; }
        virtual public float Start { get; set; }
        virtual public float End { get; set; }
        virtual public Vector2 BulletAdjustment { get; set; }
        virtual public bool DamagesEnemies { get; set; }
        virtual public Rectangle HitBox { get; set; }

        public abstract void Draw(SpriteBatch sb);

        public abstract void Initialize(Game game);

        public abstract void Move();

        public abstract List<Entity> Shoot(GameTime gt, KeyboardState cks);

        public abstract void TakeDamage(float d);

        public abstract Entity ApplyEffect(Entity e); 
    }
}
