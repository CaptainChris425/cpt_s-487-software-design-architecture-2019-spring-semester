﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Builder;
using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Movement;
using CptS_487_game.Interfaces.Shooting;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CptS_487_game.Objects
{
    abstract class EntityDecorated : Entity
    {
        protected Entity _entity;
        public override bool Active {  get => _entity.Active;  set => _entity.Active = value; }
        public override float Speed {  get => _entity.Speed; set => _entity.Speed = value; }
        public override int Width { get => _entity.Width; set => _entity.Width = value; }
        public override int Height {  get => _entity.Height; set => _entity.Height = value; }
        public override Vector2 Position { get => _entity.Position; set => _entity.Position = value; }
        public override Game Game { get => _entity.Game;  set => _entity.Game = value; }
        public override SoundEffectInstance FireSound { get => _entity.FireSound; set => _entity.FireSound = value; }
        public override TimeSpan RateOfBullets { get => _entity.RateOfBullets; set => _entity.RateOfBullets = value; }
        public override Texture2D Texture { get => _entity.Texture; set => _entity.Texture = value; }
        public override IShooter Shooter { get => _entity.Shooter; set => _entity.Shooter = value; }
        public override IMovable Movable { get => _entity.Movable; set => _entity.Movable = value; }
        public override EntityDirector EntityDirector { get => _entity.EntityDirector; set => _entity.EntityDirector = value; }
        public override float Health {  get => _entity.Health; set => _entity.Health = value; }
        public override float Damage {  get => _entity.Damage; set => _entity.Damage = value; }
        public override int ViewPortHeight { get => _entity.ViewPortHeight; set => _entity.ViewPortHeight = value; }
        public override int ViewPortWidth {  get => _entity.ViewPortWidth;  set => _entity.ViewPortWidth = value; }
        public override float Start { get => _entity.Start; set => _entity.Start = value; }
        public override float End { get => _entity.End; set => _entity.End = value; }
        public override Vector2 BulletAdjustment { get => _entity.BulletAdjustment; set => _entity.BulletAdjustment = value; }
        public override bool DamagesEnemies { get => _entity.DamagesEnemies; set => _entity.DamagesEnemies = value; }
        public override Rectangle HitBox { get => _entity.HitBox; set => _entity.HitBox = value; }

        protected EntityDecorated(Entity entity)
        {
            _entity = entity;
        }

        public override void Draw(SpriteBatch sb)
        {
            _entity.Draw(sb);
        }

        public override void Initialize(Game game)
        {
            _entity.Initialize(game);
        }

        public override void Move()
        {
            _entity.Move();
        }

        public override List<Entity> Shoot(GameTime gt, KeyboardState cks)
        {
            return _entity.Shoot(gt, cks);
        }

        public override void TakeDamage(float d)
        {
            _entity.TakeDamage(d);
        }

        public override Entity ApplyEffect(Entity e)
        {
            return _entity.ApplyEffect(e);
        }


    }
}
