﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorDesignBlackBullet : DecoratorDesign
    {
        Texture2D _decoratedTexture;

        public DecoratorDesignBlackBullet(Entity entity) : base(entity)
        {
            //This is where the decoration happens
            //we change the texture to a different texture but keep everything else about the entity the same
            _decoratedTexture = entity.Game.Content.Load<Texture2D>("bullet");
        }

        public override Rectangle HitBox
        {
            get => new Rectangle((int)Position.X, (int)Position.Y, 20, 10);
            set => HitBox = value;
        }

        public override void Draw(SpriteBatch sb)
        {
            sb.Draw(_decoratedTexture, _entity.Position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
    }
}
