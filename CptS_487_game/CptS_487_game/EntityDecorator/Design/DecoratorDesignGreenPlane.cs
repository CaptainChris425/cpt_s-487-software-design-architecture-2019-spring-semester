﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorDesignGreenPlane : DecoratorDesign
    {
        Texture2D _decoratedTexture;
        public DecoratorDesignGreenPlane(Entity entity) : base(entity)
        {
            //This is where the decoration happens
            //we change the texture to a different texture but keep everything else about the entity the same
            _decoratedTexture = entity.Game.Content.Load<Texture2D>("spritelib_plane");
            BulletAdjustment = new Vector2(8,-19);
        }
        public override Rectangle HitBox
        {
            get => new Rectangle((int)Position.X + 10, (int)Position.Y, 5, 5);
            set => HitBox = value;
        }

        public override void Draw(SpriteBatch sb)
        {
            sb.Draw(_decoratedTexture, _entity.Position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
    }
}
