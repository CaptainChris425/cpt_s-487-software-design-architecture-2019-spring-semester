﻿using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorDesignSantaFinalBoss : DecoratorDesign
    {
        Texture2D _decoratedTexture;

        public DecoratorDesignSantaFinalBoss(Entity entity) : base(entity)
        {
            //This is where the decoration happens
            //we change the texture to a different texture but keep everything else about the entity the same
            _decoratedTexture = entity.Game.Content.Load<Texture2D>("santa_02");
        }

        public override void Draw(SpriteBatch sb)
        {
            sb.Draw(_decoratedTexture, _entity.Position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
    }
    }
