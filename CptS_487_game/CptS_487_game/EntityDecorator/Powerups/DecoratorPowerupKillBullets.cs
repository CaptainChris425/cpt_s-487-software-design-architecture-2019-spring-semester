﻿using CptS_487_game.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorPowerupKillBullets : DecoratorPowerup
    {
        public DecoratorPowerupKillBullets(Entity entity) : base(entity)
        {
            entity.DamagesEnemies = true;
        }
        public override Entity ApplyEffect(Entity e)
        {
            if(e.Health > 1000)
            {
                e.Active = false;
            }
            else
            {
                e.Health -= Damage;
            }



            return e;
        }

    }
}
