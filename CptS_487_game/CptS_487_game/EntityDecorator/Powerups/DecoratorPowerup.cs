﻿using CptS_487_game.Interfaces.Entity;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.EntityDecorator
{
    abstract class DecoratorPowerup : EntityDecorated
    {
        public DecoratorPowerup(Entity entity) : base(entity)
        {
        }

        public abstract override Entity ApplyEffect(Entity e);
    }
}
