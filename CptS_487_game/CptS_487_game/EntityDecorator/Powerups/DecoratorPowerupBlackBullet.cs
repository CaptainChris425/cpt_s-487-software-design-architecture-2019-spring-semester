﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Objects;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorPowerupBlackBullet : DecoratorPowerup
    {
        public DecoratorPowerupBlackBullet(Entity entity) : base(entity)
        {
        }

        public override Entity ApplyEffect(Entity e)
        {
            e = new DecoratorShooterBlackBullet(e);
            _entity.Active = false;
            return e;
        }
    }
}
