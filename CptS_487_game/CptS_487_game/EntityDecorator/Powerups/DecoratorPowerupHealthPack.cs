﻿using CptS_487_game.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorPowerupHealthPack : DecoratorPowerup
    {
        private float _decoratedDamage;
        public DecoratorPowerupHealthPack(Entity entity) : base(entity)
        {
            _decoratedDamage = 1;
        }

        public override float Damage { get => _decoratedDamage; set => _decoratedDamage = value; }

        public override Entity ApplyEffect(Entity e)
        {
            e.Health += _decoratedDamage;
            Active = false;
            return e;
        }

    }
}
