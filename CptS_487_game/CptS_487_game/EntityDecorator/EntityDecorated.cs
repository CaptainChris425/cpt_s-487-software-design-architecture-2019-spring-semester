﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Movement;
using CptS_487_game.Interfaces.Shooting;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CptS_487_game.EntityDecorator
{
    abstract class EntityDecorated : Entity
    {
        protected Entity _entity;

        protected EntityDecorated(Entity entity)
        {
            _entity = entity;
        }

        public override void AddActive(bool active)
        {
            _entity.AddActive(active);
        }

        public override void AddHealth(float health)
        {
            _entity.AddHealth(health);
        }

        public override void AddMovement(IMovable movement)
        {
            _entity.AddMovement(movement);
        }

        public override void AddPosition(Vector2 vector)
        {
            _entity.AddPosition(vector);
        }

        public override void AddRateOfBullets(TimeSpan rate)
        {
            _entity.AddRateOfBullets(rate);
        }

        public override void AddShooter(IShooter shooter)
        {
            _entity.AddShooter(shooter);
        }

        public override void AddSpeed(float speed)
        {
            _entity.AddSpeed(speed);
        }

        public override void AddTexture(Texture2D texture)
        {
            _entity.AddTexture(texture);
        }

        public override void AddViewPortHeight(int height)
        {
            _entity.AddViewPortHeight(height);
        }

        public override void AddViewPortWidth(int width)
        {
            _entity.AddViewPortWidth(width);
        }

        public override void AddDamage(float d)
        {
            _entity.AddDamage(d);
        }

        public override float DealDamage()
        {
            return _entity.DealDamage();
        }

        public override void Draw(SpriteBatch sb)
        {
            _entity.Draw(sb);
        }

        public override void Initialize(Game game)
        {
            _entity.Initialize(game);
        }

        public override void Move()
        {
            _entity.Move();
        }

        public override IEntityBuilder Shoot(GameTime gt, KeyboardState cks)
        {
            return _entity.Shoot(gt, cks);
        }

        public override void TakeDamage(float d)
        {
            _entity.TakeDamage(d);
        }

        public override bool Active
        {
            get { return _entity.Active; }
        }
        public override float Speed //constant speed
        {
            get { return _entity.Speed; }
        }
        public override int Width
        {
            get { return _entity.Width; }
        }
        public override int Height
        {
            get { return _entity.Height; }
        }
        public override Vector2 Position
        {
            get { return _entity.Position; }
        }
        public override Game Game
        {
            get { return _entity.Game; }
        }
        public override TimeSpan RateOfBullets
        {
            get { return _entity.RateOfBullets; }
        }
        public override Texture2D Texture {
            get { return _entity.Texture; }
        }
    }
}
