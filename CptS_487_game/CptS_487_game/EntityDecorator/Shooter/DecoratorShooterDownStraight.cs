﻿using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorShooterDownStraight : DecoratorShooter
    {
        //IShooter _shooter;
        //ShooterFactory shooterFactory;

        public DecoratorShooterDownStraight(Entity entity) : base(entity)
        {
            //shooterFactory = new ShooterFactory();
            //_shooter = shooterFactory.CreateProduct("Enemy");
        }

        public override List<Entity> Shoot(GameTime gt, KeyboardState cks)
        {
            List<Entity> decoratedBullets = new List<Entity>();
            //Entity bullet = null;
            //IEntityBuilder entityBuilder = _shooter.Shoot(gt, cks, _entity.RateOfBullets);
            //if (entityBuilder != null) {
            //    _entity.EntityDirector.Construct(entityBuilder, _entity.Position);
            //    bullet = entityBuilder.GetResult();
            //}
            List<Entity> basic = _entity.Shoot(gt, cks);
            double numberOfTicks = 0;

            if (basic != null)
            {
                foreach (Entity e in basic)
                {
                    decoratedBullets.Add(new DecoratorMovementDownStraight(e));
//fsfd
                    Vector2 Pos = e.Position;
                    Pos.Y = (float)(250*Math.Sin(numberOfTicks * 0.5 * Math.PI));
                    Pos.X += e.Speed;
                    numberOfTicks++;
                    
                    //dsff
                }
                return decoratedBullets;
            }
            return basic;
        }
    }
}
