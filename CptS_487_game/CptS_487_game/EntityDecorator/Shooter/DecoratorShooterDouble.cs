﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorShooterDouble : DecoratorShooter
    {
        public DecoratorShooterDouble(Entity entity) : base(entity)
        {
        }

        public override List<Entity> Shoot(GameTime gt, KeyboardState cks)
        {
            List<Entity> temp = _entity.Shoot(gt, cks);
            List<Entity> decoratedBullets = new List<Entity>();
            if (temp != null)
            {
                if (temp.Count > 0)
                {
                    
                    Entity duplicate = temp[0];
                    decoratedBullets.Add(duplicate);
                    Entity duplicate2 = duplicate;
                    decoratedBullets.Add(duplicate2);
                    decoratedBullets[0].Position = new Vector2(_entity.Position.X + 13, _entity.Position.Y - 19);
                    decoratedBullets[1].Position = new Vector2(_entity.Position.X + 3, _entity.Position.Y - 19);
                    return decoratedBullets;
                }
                else { return null; }
            }
            return temp;
        }

        

    }
}
