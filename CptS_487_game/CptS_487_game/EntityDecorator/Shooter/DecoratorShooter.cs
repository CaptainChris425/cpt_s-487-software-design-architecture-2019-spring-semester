﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CptS_487_game.EntityDecorator
{
    abstract class DecoratorShooter : EntityDecorated
    {
        public DecoratorShooter(Entity entity) : base(entity)
        {
        }

        public abstract override List<Entity> Shoot(GameTime gt, KeyboardState cks);
    }
}
