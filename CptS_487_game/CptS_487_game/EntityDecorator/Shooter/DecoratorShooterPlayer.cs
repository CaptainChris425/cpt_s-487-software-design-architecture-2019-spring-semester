﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorShooterPlayer : DecoratorShooter
    {
        public DecoratorShooterPlayer(Entity entity) : base(entity)
        {
        }

        public override List<Entity> Shoot(GameTime gt, KeyboardState cks)
        {
            //This is the base shooting for player .
            //It only adjusts the bullets positions to be in a more accurate position for how the plyer model should actually shoot
            List<Entity> decoratedBullets = _entity.Shoot(gt, cks);
            if (decoratedBullets!= null)
                decoratedBullets[0].Position = new Vector2(_entity.Position.X+8,_entity.Position.Y-19);
            return decoratedBullets;
        }
    }
}
