﻿using CptS_487_game.Builder;
using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Shooting;
using CptS_487_game.MovementFactory.Shooting;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorShooterSecretFeature : DecoratorShooter
    {
        //IShooter _shooter;
        //ShooterFactory shooterFactory;
        TimeSpan _previousBulletSpawnTime;
        int walls;
        int numberOfWalls = 5;
        bool done;
        IShooter shooter;
        int maxX;
        public DecoratorShooterSecretFeature(Entity entity) : base(entity)
        {
            //shooterFactory = new ShooterFactory();
            //_shooter = shooterFactory.CreateProduct("Enemy");
            walls = 0;
            maxX = Game.GraphicsDevice.Viewport.Width;
            shooter = new ShooterNotTimed();
            done = false;
        }

        public override List<Entity> Shoot(GameTime gt, KeyboardState cks)
        {
            List<Entity> shot = new List<Entity>();
            if (_previousBulletSpawnTime == default(TimeSpan)) _previousBulletSpawnTime = gt.TotalGameTime;
            if (gt.TotalGameTime - _previousBulletSpawnTime > RateOfBullets && walls < numberOfWalls)
            {
                walls++;
                _previousBulletSpawnTime = gt.TotalGameTime;
                for (float i = 0; i < maxX; i += (float)(.05 * maxX))
                {
                    IEntityBuilder bulletBuilder = new EnemyBulletBuilder();
                    EntityDirector.Construct(bulletBuilder, new Vector2(i, Position.Y+50));
                    Entity bullet = bulletBuilder.GetResult();
                    bullet.Speed = 1;
                    shot.Add(new DecoratorDesignYellowBall(new DecoratorMovementDownStraight(bullet)));
                }
                return shot;

            }else if(gt.TotalGameTime - _previousBulletSpawnTime > RateOfBullets && walls >= numberOfWalls && !done)
            {
                done = true;
                for (float i = (float)(.25 * maxX); i <= maxX; i += (float)(.25 * maxX))
                {
                    IEntityBuilder bulletBuilder = new EnemyBulletBuilder();
                    EntityDirector.Construct(bulletBuilder, new Vector2(i, Position.Y + 50));
                    Entity bullet = bulletBuilder.GetResult();
                    bullet.Speed = 2;
                    shot.Add(new DecoratorDesignBlackBullet(new DecoratorPowerupKillBullets(new DecoratorMovementDownStraight(bullet))));
                }
                return shot;

            }
            return null;
        }
    }
}
