﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Shooting;
using CptS_487_game.MovementFactory;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorShooterNone : DecoratorShooter
    {
        //IShooter _shooter;
        //ShooterFactory shooterFactory;
        TimeSpan decoratedRateOfBullets;
        public DecoratorShooterNone(Entity entity) : base(entity)
        {

        }

        public override List<Entity> Shoot(GameTime gt, KeyboardState cks)
        {
            return null;
        }
    }
}
