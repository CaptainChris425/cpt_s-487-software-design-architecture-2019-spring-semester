﻿using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Entity;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.EntityDecorator
{
    abstract class EntityTEMPORARY : IEntityMoveable, IEntityShootable, IEntityDrawable
    {
        public Game _game;
        public Vector2 _position;
        public float _speed;

        public abstract void Draw(SpriteBatch sb);

        public abstract void Move(KeyboardState cks);

        public void Move()
        {
            throw new NotImplementedException();
        }

        public abstract void Shoot(GameTime gt, KeyboardState cks);

        IEntityBuilder IEntityShootable.Shoot(GameTime gt, KeyboardState cks)
        {
            throw new NotImplementedException();
        }
    }
}
