﻿using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Movement;
using CptS_487_game.Interfaces.Shooting;
using CptS_487_game.MovementFactory;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorMovementControlled : DecoratorMovement
    {
        MovementCreator movementFactory;
        IMovable movement;

        public DecoratorMovementControlled(Entity entity) : base(entity)
        {
            movementFactory = new MovementCreator();

            movement = movementFactory.CreateProduct("Controlled");
        }

        public override void Move()
        {
            Position = movement.Move(Game, Texture, Position, Speed);
        }
    }
}
