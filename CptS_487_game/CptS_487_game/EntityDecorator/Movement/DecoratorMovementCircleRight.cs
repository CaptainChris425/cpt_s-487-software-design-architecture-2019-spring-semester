﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Movement;
using CptS_487_game.Interfaces.Shooting;
using CptS_487_game.MovementFactory;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;


namespace CptS_487_game.EntityDecorator
{
    class DecoratorMovementCircleRight : DecoratorMovement
    {
        int i, j;
        public DecoratorMovementCircleRight(Entity entity) : base(entity)
        {
            i = 0;
            j = 0;
        }

        public override void Move()
        {
            //_entity.Position = movement.Move(_entity.Game, _entity.Texture, _entity.Position, _entity.Speed);
            i++;
            if (i % 5 == 0) j++;
            Position = new Vector2((float)Math.Sin(j) * Speed * 5 + Position.X,(float)Math.Cos(j) * Speed * 5 + Position.Y);

        }
    }
}
