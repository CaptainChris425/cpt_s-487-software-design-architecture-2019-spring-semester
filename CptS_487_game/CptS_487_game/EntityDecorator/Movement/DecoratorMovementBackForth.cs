﻿using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Movement;
using CptS_487_game.Interfaces.Shooting;
using CptS_487_game.MovementFactory;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorMovementBackForth : DecoratorMovement
    {
        MovementCreator movementFactory;
        IMovable movement;

        public DecoratorMovementBackForth(Entity entity) : base(entity)
        {
            movementFactory = new MovementCreator();

            movement = movementFactory.CreateProduct("BackForth");
           
        }

        public override void Move()
        {
            _entity.Position = movement.Move(_entity.Game, _entity.Texture, _entity.Position, _entity.Speed);
        }
    }
}
