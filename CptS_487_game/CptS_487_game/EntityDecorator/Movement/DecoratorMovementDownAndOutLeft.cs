﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces;
using CptS_487_game.Interfaces.Movement;
using CptS_487_game.Interfaces.Shooting;
using CptS_487_game.MovementFactory;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorMovementDownAndOutLeft : DecoratorMovement
    {
        double maxX;
        double maxY;
        double turnPoint;
        public DecoratorMovementDownAndOutLeft(Entity entity) : base(entity)
        {
            maxX = _entity.Game.GraphicsDevice.Viewport.Width - 30;
            maxY = _entity.Game.GraphicsDevice.Viewport.Height;
            turnPoint = maxY * 0.1;
        }

        public override void Move()
        {
            if (Position.Y < turnPoint)
            {
                _entity.Position = new Vector2(_entity.Position.X, _entity.Position.Y + Speed);
            }
            else
            {
                _entity.Position = new Vector2(_entity.Position.X - Speed, _entity.Position.Y);
            }

        }
    }
}
