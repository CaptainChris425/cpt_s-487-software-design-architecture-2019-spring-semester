﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Interfaces.Movement;
using CptS_487_game.MovementFactory;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework.Input;

namespace CptS_487_game.EntityDecorator
{
    class DecoratorMovementStayPut : DecoratorMovement
    {
        MovementCreator movementFactory;
        IMovable movement;

        public DecoratorMovementStayPut(Entity entity) : base(entity)
        {
            movementFactory = new MovementCreator();

            movement = movementFactory.CreateProduct("StayPut");
        }

        public override void Move()
        {
            _entity.Position = movement.Move(_entity.Game, _entity.Texture, _entity.Position, _entity.Speed);
        }
    }
}
