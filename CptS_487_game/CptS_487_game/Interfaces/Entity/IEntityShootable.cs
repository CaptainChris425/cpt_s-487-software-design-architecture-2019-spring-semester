﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CptS_487_game.Objects;
using CptS_487_game.Builder;

namespace CptS_487_game.Interfaces.Entity
{
    interface IEntityShootable
    {
        EntityDirector EntityDirector { get; set; }
        List<Objects.Entity> Shoot(GameTime gt, KeyboardState cks);
    }
}
