﻿using CptS_487_game.Builder;
using CptS_487_game.Interfaces.Movement;
using CptS_487_game.Interfaces.Shooting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Interfaces.Entity
{
    //The interface that an entity will need to impliment in order
        //to be built by one of the entitybuilders
    interface IEntityBuildable
    {
        void Initialize(Game game);
        Texture2D Texture { get; set; }     //To add the texture 
        Vector2 Position { get; set; }       //To set the position
        bool Active { get; set; }          //To set the acitve status
        TimeSpan RateOfBullets { get; set; }   //To set the rate of bullets
        float Health { get; set; }           //To set the health
        int Width { get; set; }
        int Height { get; set; }         
        float Speed { get; set; }             //To add speed
        IShooter Shooter { get; set; }
        IMovable Movable { get; set; }
        int ViewPortHeight { get; set; }       //To set the ViewPort width
        int ViewPortWidth { get; set; }         //To set the ViewPort height
        float Damage { get; set; }
    }
}
