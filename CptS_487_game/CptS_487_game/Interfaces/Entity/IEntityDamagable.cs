﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Interfaces.Entity
{
    interface IEntityDamagable
    {
        void TakeDamage(float d); //If health goes to 0 set active to false
        float Damage { get;}
    }
}
