﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Interfaces.Entity
{
    interface IEntityDrawable
    {
        void Draw(SpriteBatch sb);
    }
}
