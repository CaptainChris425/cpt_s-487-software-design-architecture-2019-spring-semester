﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Interfaces.Movement
{
    interface IMovable
    {
        Vector2 Move(Game game,Texture2D texture, Vector2 position, float speed);
    }
}
