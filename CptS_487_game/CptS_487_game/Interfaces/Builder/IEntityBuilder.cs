﻿using CptS_487_game.Interfaces.Entity;
using CptS_487_game.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Interfaces
{
    //Abstract Builder interface. This will build an IBuildableEntity.
    interface IEntityBuilder
    {
        void Initialize(Game game);
        void BuildSpeed();                                  //To add speed to the Entity
        void BuildTexture(ContentManager device);           //To add the texture to the entity. 
        void BuildPosition(Vector2 position);               //To add the position to the entity.
        void BuildActive();                                 //To set the active status of the entity.
        void BuildRateOfBullets();                          //To set the rate of bullets for the entity.
        void BuildHealth();                                 //To set the Health of the entity.
        void BuildViewPortHeight(GraphicsDevice device);    //To set the ViewPort height of the entity.
        void BuildViewPortWidth(GraphicsDevice device);     //To add the ViewPort width of the entity.
        void BuildMovement();                               //To add the movement
        void BuildShooter();
        void BuildDamage();
        void BuildEntityDirector(Game game);
        ConcreteEntity GetResult();                         //Returns the built entity.
    }
}
