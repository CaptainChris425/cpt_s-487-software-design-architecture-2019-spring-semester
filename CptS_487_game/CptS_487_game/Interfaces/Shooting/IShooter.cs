﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CptS_487_game.Interfaces.Shooting
{
    interface IShooter
    {
        IEntityBuilder Shoot(GameTime gt, KeyboardState cks, TimeSpan rateOfFire);
    }
}
