﻿using System;
using System.Collections.Generic;
using System.Text;
using PeaShooterGame.Zombies;
using System.Threading;

namespace PeaShooterGame.Maps
{
    public class SandBoxMap : Map
    {
        int ZombieCount;
        List<Zombie> zombies;
        ZombieFactory standardZombieFactory;
        int peaShooterDamage;
        int bulletPosition = 0;
        int boardSize = 0;

        public SandBoxMap()
        {
            standardZombieFactory = new StandardZombieFactory();
            zombies = new List<Zombie>();
        }

        public void Banner()
        {
            Console.WriteLine("====================\nCopy of PvZ\n====================");
        }

        public override void RunMap()
        {
            Banner();
            int selection;
           // Console.WriteLine("What type of game would you like to play?");
            Console.WriteLine("\t1.Create Zombies");
           // Console.WriteLine("\t2.SandBox Mode");
            Int32.TryParse(Console.ReadLine(), out selection);
            if (selection == 1)
            {
                SetUp();
            }

            Console.WriteLine("What would you like to do?");
            Console.WriteLine("\t1.Create Zombies");
            Console.WriteLine("\t2.SandBox Mode");
            Int32.TryParse(Console.ReadLine(), out selection);
            while (selection != 2)
            {
                SetUp();
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("\t1.Create Zombies");
                Console.WriteLine("\t2.SandBox Mode");
                Int32.TryParse(Console.ReadLine(), out selection);
            }
            if (selection == 2)
            {
                Console.WriteLine("====================\nGameTime\n====================");
                Console.WriteLine("\t1.Make one move");
                Console.WriteLine("\t2.Finish the game");
                Console.WriteLine("");
                while (zombies.Count > 0)
                {
                    PrintZombies();
                    Int32.TryParse(Console.ReadLine(), out selection);
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    ClearLine();
                    if (selection == 1)
                    {
                        zombies[0].TakeDamage(peaShooterDamage);
                        zombies.RemoveAll(x => !x.Alive);
                    }
                    if (selection == 2)
                    {
                        while (zombies.Count > 0)
                        {
                            PrintZombies();
                            zombies[0].TakeDamage(peaShooterDamage);
                            zombies.RemoveAll(x => !x.Alive);
                            Thread.Sleep(750);
                        }
                    }
                }

            }
            PrintZombies();
            Console.WriteLine("Game Over...");
            Console.ReadLine();

        }

        static void ClearLine()
        {
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, Console.CursorTop - 1);
        }

        void PrintZombies()
        {
            
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            ClearLine();
            StringBuilder board = new StringBuilder();
            board.Append("|,,(*)p,,");
            if(zombies.Count == 0) { board.Append(",,,,,,,"); bulletPosition = -1; }
            if (bulletPosition == 0) { board.Append("*,,,,,,"); bulletPosition = 1; }
            else if (bulletPosition == 1) {board.Append(",,,,*,,"); bulletPosition = 2; }
            else if (bulletPosition == 2) { board.Append("*,,,,,*"); bulletPosition = 3; }
            else if (bulletPosition == 3) { board.Append(",,*,,,,"); bulletPosition = 0; }
            foreach (Zombie zombie in zombies)
            {
                board.Append(",,");
                board.Append(zombie.Draw());
                board.Append(",,");
            }
            if (boardSize == 0) boardSize = board.Length;
            while (board.Length < boardSize) board.Append(",");
            board.Append("|\n");
            Console.Write(board.ToString());
        }

        void SetDamage()
        {
            int selection;
            do
            {
                Console.WriteLine("Enter a damage value:");
            } while (!Int32.TryParse(Console.ReadLine(), out selection));

            peaShooterDamage = selection;
        }

        void SetZombies()
        {
            int selection;
            
            for (int i = 0; i < ZombieCount; i++)
            {
                Zombie z = null;
                //do
                //{
                //    AskZombieSelection(i + 1);
                //} while (!Int32.TryParse(Console.ReadLine(), out selection));

                //Zombie z = GetZombieFromSelection(selection);

                while (z is null)
                {
                    do
                    {
                        AskZombieSelection(i + 1);
                    } while (!Int32.TryParse(Console.ReadLine(), out selection));

                    z = GetZombieFromSelection(selection);
                }

                zombies.Add(z);
            }
            Console.WriteLine();
        }

        void AskZombieSelection(int i)
        {
            Console.WriteLine("[%d] Which Zombie would you like to add?:", i);
            Console.WriteLine("\t1.Regular");
            Console.WriteLine("\t2.Cone");
            Console.WriteLine("\t3.Bucket");
            Console.WriteLine("\t4.ScreenDoor");
        }

        Zombie GetZombieFromSelection(int selection)
        {
            switch (selection)
            {
                case 1:
                    return standardZombieFactory.GetZombie("standard");
                case 2:
                    return standardZombieFactory.GetZombie("cone");
                case 3:
                    return standardZombieFactory.GetZombie("bucket");
                case 4:
                    return standardZombieFactory.GetZombie("screen");
                default:
                    return null;
            }
        }
        void SetZombieCount()
        {
            do
            {
                Console.WriteLine("Enter how many zombies to spawn (as an integer): ");
            } while (!Int32.TryParse(Console.ReadLine(), out ZombieCount));
        }

        public override void SetUp()
        {
            if (peaShooterDamage == 0) SetDamage();
            SetZombieCount();
            SetZombies();
        }
    }
}
