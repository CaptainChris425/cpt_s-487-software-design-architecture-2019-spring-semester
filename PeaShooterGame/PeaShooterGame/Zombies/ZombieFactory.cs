﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PeaShooterGame.Zombies
{
    public abstract class ZombieFactory
    {
        public abstract Zombie GetZombie(string zombie);
    }
}
