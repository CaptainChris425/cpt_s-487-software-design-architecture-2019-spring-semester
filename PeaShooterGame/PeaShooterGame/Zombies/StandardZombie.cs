﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PeaShooterGame.Zombies
{
    class StandardZombie : Zombie
    {
        List<Zombie> zombies = new List<Zombie>();

        public StandardZombie()
        {
            Health = 50;
            Alive = true;
            Name = "Standard Zombie";
        }


        public override int Health { get => health;  set => health = value;  }


        public override void Add(Zombie zombie)
        {
            zombies.Add(zombie);
        }

        public override void Die()
        {
            Alive = false;
        }

        public override string Draw()
        {
            if (zombies.Count > 0)
            {
                return zombies[0].Draw();
            }
            else
            {
                StringBuilder draw = new StringBuilder();
                draw.Append(",=|*.|-");
                draw.Append(Health);
                draw.Append("-,");
                return draw.ToString();
            }
        }

        public override Zombie GetChild(int i)
        {
            return zombies[i];
        }

        public override void Remove(Zombie zombie)
        {
            zombies.Remove(zombie);
        }

        public override int TakeDamage(int d)
        {
            //Damage only occurs to the outermost part. 

            /*My idea if there were to be specific parts to block
             * I would change TakeDamage(int d) to TakeDamage(int d, string position) 
             * this would allow me to incorporate separate zombie lists for each part of
             * the body and position would specify which part (list) is it damaging
             * for each part of the list is empty or any extra damage will damage the 
             * standardzombie directly
             */

            while(zombies.Count > 0 && d > 0)           //While there is zombies attached to the zombie and there is damage to be done
            {
                d = zombies[0].TakeDamage(d);           //Take damage and return the extra damage as d
                if (zombies[0].Alive) return 0;         //if the armor is still alive the damage is done
                else                                    //the armor is dead
                {
                    Remove(zombies[0]);                 //Get rid of the armor, making the list count 0 or the next armor at index 0
                }
            }
            Health -= d;                                //Take the eccess damage straight to the standardZombie
            if (Health <= 0)                            //if health below 0
                Die();                                  //Die

            return 0;                                   //Return for end of function
        }
    }
}
