﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment5
{
    public class FlipDownCommand : ICommand
    {
        private Light theLight;

        public FlipDownCommand(Light light)
        {
            theLight = light;
        }

        public void Execute()
        {
            theLight.TurnOff();
        }
    }
}
