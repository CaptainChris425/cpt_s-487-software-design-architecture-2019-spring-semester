﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment5
{
    public class FlipUpCommand : ICommand
    {
        private Light theLight;

        public FlipUpCommand(Light light)
        {
            theLight = light;
        }

        public void Execute()
        {
            theLight.TurnOn();
        }
    }
}
