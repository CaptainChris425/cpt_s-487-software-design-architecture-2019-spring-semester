﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment5
{
    class Program
    {
        static void Main(string[] args)
        {
            Light bathroomLight = new Light();
            FlipDownCommand flipDown = new FlipDownCommand(bathroomLight);
            FlipUpCommand flipUp = new FlipUpCommand(bathroomLight);
            Switch flipper = new Switch();
            flipper.StoreAndExecute(flipUp);
            flipper.StoreAndExecute(flipDown);
            Console.Read();

        }
    }
}
