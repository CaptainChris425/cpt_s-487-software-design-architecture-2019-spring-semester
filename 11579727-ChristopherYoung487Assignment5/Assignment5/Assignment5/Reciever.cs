﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment5
{
    public class Light
    {
        public void TurnOn()
        {
            Console.WriteLine("This light is on\n");
        }

        public void TurnOff()
        {
            Console.WriteLine("This light is off\n");
        }
    }
}
