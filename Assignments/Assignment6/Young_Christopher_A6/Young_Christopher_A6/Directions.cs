﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Young_Christopher_A6
{

    public class Constants
    {
        public const int FRONT = 0;
        public const int TOP = 1;
        public const int BEHIND = 2;
        public const int BOTTOM = 3;
        public const int PEASHOOTER = 1;
        public const int WATERMELON = 2;
        public const int MAGNETSHROOM = 3;
        public const int ZOMBIESTANDARD = 1;
        public const int ZOMBIEBUCKET = 2;
        public const int ZOMBIECONE = 3;
        public const int ZOMBIESCREENDOOR = 4;


        private static Constants direction;
        private Constants()
        {

        }

        public static Constants GetDirection()
        {
            if (direction == null)
            {
                direction = new Constants();
            }
            return direction;
        }


    }
}
