﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Young_Christopher_A6.Controllers;

namespace Young_Christopher_A6
{
    class PlantsVsZombies
    {
        GameEventManager gameEventManager = new GameEventManager();
        int GetSelection()
        {
            return Int32.Parse(Console.ReadLine());
        }

        void MainMenuPrompt()
        {
            Console.WriteLine("1. Create Zombies");
            Console.WriteLine("2. Demo game play");
        }

        void AttackPrompt()
        {
            Console.WriteLine("Which type of attack to perform?");
            Console.WriteLine("1. PeaShooter");
            Console.WriteLine("2. WaterMelon");
            Console.WriteLine("3. MagnetShroom");
            Console.WriteLine("4. END THE GAME");
        }
        
        void CreateZombiesPrompt()
        {
            Console.WriteLine("Which Zombie to add?");
            Console.WriteLine("1. Standard");
            Console.WriteLine("2. Bucket");
            Console.WriteLine("3. Cone");
            Console.WriteLine("4. ScreenDoor");
            Console.WriteLine("5. Go Back to menu");
        }

        void CreateZombies()
        {
            int selection = 0;
            while (selection != 5)
            {
                CreateZombiesPrompt();
                selection = GetSelection();
                if(selection < 5 && selection > 0)
                    gameEventManager.AddZombie(selection);
            }

        }

        void DemoGamePlay()
        {
            int selection = 0;
            while(selection != 4 && selection != -1)
            {
                AttackPrompt();
                selection = GetSelection();
                selection = gameEventManager.SimulateCollisionDetection(selection);
            }


            gameEventManager.ResetGame();
        }

        public void MainMenu()
        {
            while (true)
            {
                MainMenuPrompt();
                switch (GetSelection())
                {
                    case (1):
                        CreateZombies();
                        break;
                    case (2):
                        DemoGamePlay();
                        break;
                }
            }
        }

    }
}
