﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Young_Christopher_A6.Zombies;
using Young_Christopher_A6.Zombies.Decorators;

namespace Young_Christopher_A6.Factories
{
    class StandardZombieFactory : ZombieFactory
    {
        public override IZombie GetZombie(int zombie)
        {
            switch (zombie)
            {
                case (Constants.ZOMBIESTANDARD):
                    return new ZombieStandard();
                case (Constants.ZOMBIEBUCKET):
                    return new ZombieBucket(new ZombieStandard());
                case (Constants.ZOMBIECONE):
                    return new ZombieCone(new ZombieStandard());
                case (Constants.ZOMBIESCREENDOOR):
                    return new ZombieScreenDoor(new ZombieStandard());
                default:
                    return null;
                
            }
        }
    }
}
