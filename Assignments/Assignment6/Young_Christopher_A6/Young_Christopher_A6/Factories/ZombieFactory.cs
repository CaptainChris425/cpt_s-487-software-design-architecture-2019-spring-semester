﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Young_Christopher_A6.Zombies;

namespace Young_Christopher_A6.Factories
{
    abstract class ZombieFactory
    {
        public abstract IZombie GetZombie(int zombie);
    }
}
