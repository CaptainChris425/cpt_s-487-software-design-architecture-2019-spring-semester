﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Young_Christopher_A6.Zombies;
using Young_Christopher_A6;
using Young_Christopher_A6.Factories;

namespace Young_Christopher_A6.Controllers
{
    class GameEventManager
    {
        GameObjectManager game = new GameObjectManager();
        ZombieFactory zombieFactory = new StandardZombieFactory();
        //Called when collision is detected between a Bullet b and an Enemy e
        //In a real implementation, the operation will get the damage from the
        //Bullet b. For instance:
        //public void doDamage(Bullet b, Enemy e)
        //{
        // int damage = b.getDamage();
        // e.takeDamage(damage);
        //}
        //For this test, simply pass in the damage of the Peashooter/Watermelon
        //to the following operation (25 or 30, specifically)
        public void DoDamage(int d, IZombie e)
        {
            e.TakeDamage(Constants.FRONT,d);
        }

        public void DoDamageFromAbove(int d, IZombie e)
        {
            e.TakeDamage(Constants.TOP, d);
        }
        //Called when "collision" is detected between
        //a magnet-shroom and an Enemy e
        //i.e, when the user select the magnet-shroom attack.
        public void ApplyMagnetForce(IZombie e)
        {
            //If it's a metal accessory, we need to remove the “metal” accessory
            //from e. How? It’s up to you
            //TODO: complete this method.
            //Hint: a simple type-check is necessary here.
            if (e.IsMetal) e.TakeDamage(Constants.FRONT, e.Health);
        }
        //To separate the responsibilities, the above methods should not
        //be called directly from your code handling user-interaction.
        //Instead, it should be done in this “hub” operation in the control
        //class. Since we are simulating, pass an “int” to represent the plant.
        public int SimulateCollisionDetection(int plant)
        {
            //The method gets access to the “enemies” list in GameObjectManager
            //and finds the first Enemy to be the one to collide with.
            //Then, it passes e to one of the functions above.
            //TODO: complete this method.
            game.NotifyObservers();
            IZombie e = game.GetZombie();
            switch (plant)
            {
                case (Constants.PEASHOOTER): //Peashooter
                    DoDamage(25, e);
                    break;
                case (Constants.WATERMELON):
                    DoDamageFromAbove(30, e);
                    break;
                case (Constants.MAGNETSHROOM):
                    ApplyMagnetForce(e);
                    break;

            }
            game.NotifyObservers();
            Console.WriteLine(Draw());
            if (game.enemies.Count() > 0) return 0;
            return -1;
        }

        public void AddZombie(int zombie)
        {
            game.AddObserver(zombieFactory.GetZombie(zombie));
        }

        public string Draw()
        {
            StringBuilder str = new StringBuilder();
            str.Append(",,,,");
            foreach (IZombie z in game.enemies){
                str.Append(z.Draw());
                str.Append(",,");
            }
            return str.ToString();
        }

        public void ResetGame()
        {
            game = new GameObjectManager();
        }
    }
}
