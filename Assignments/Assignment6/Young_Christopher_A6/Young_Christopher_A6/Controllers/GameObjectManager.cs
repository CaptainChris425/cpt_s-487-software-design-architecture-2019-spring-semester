﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Young_Christopher_A6.Enemies;
using Young_Christopher_A6.Zombies;

namespace Young_Christopher_A6.Controllers
{
    class GameObjectManager
    {
        //Keep track of all the enemies.
        //Accessed by GameEventManger when calculating collision.
        //The List here is filled when the user creates the Zombies.
        public List<IZombie> enemies = new List<IZombie>();
        public IZombie GetZombie()
        {
            return enemies[0];
        }
        //TODO: Observer Pattern related attributes and methods.
        public void NotifyObservers()
        {
            for(int i = 0; i< enemies.Count(); i++)
            {
                if (enemies[i] == null) {
                    enemies.RemoveAt(i);
                    i--;
                }
                else if (!enemies[i].Alive)
                {
                    enemies.RemoveAt(i);
                    i--;
                }
            }
        }

        public void AddObserver(IZombie zombie)
        {
            enemies.Add(zombie);
        }
        public void RemoveObserver()
        {
            enemies.RemoveAt(0);
        }


    }
}
