﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Young_Christopher_A6.Zombies.Decorators
{
    abstract class ZombieDecorated : IZombie
    {
        protected IZombie _decoratedZombie;
        protected int _health;
        protected bool _alive;
        protected string _name;
        protected bool _isMetal;

        public int Health { get => _health; set => _health = value; }
        public bool Alive { get => _decoratedZombie.Alive; set => _decoratedZombie.Alive = value; }
        public string Name { get => _name; set => _name = value; }
        public bool IsMetal { get
            {
                if (SelfAlive) return _isMetal;
                return _decoratedZombie.IsMetal;
            }
            set => _isMetal = value; }
        protected IZombie Zombie { get => _decoratedZombie; set => _decoratedZombie = value; }
        protected bool SelfAlive { get => _alive; set => _alive = value; }

        public void Die()
        {
            SelfAlive = false;
        }

        abstract public string Draw();

        abstract public int TakeDamage(int Direction, int Damage);
    }
}
