﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Young_Christopher_A6.Zombies.Decorators
{
    class ZombieCone : ZombieDecorated
    {
        public ZombieCone(IZombie z)
        {
            Zombie = z;
            Health = 25;
            IsMetal = false;
            Name = "ScreenDoorZombie";
            SelfAlive = true;

        }

        public override string Draw()
        {
            if (!SelfAlive) return Zombie.Draw();
            StringBuilder draw = new StringBuilder();
            draw.Append(",=/*.\\-");
            draw.Append(Health + Zombie.Health);
            draw.Append(" -,");
            return draw.ToString();
        }

        public override int TakeDamage(int Direction, int Damage)
        {
            if (!SelfAlive)
                return Zombie.TakeDamage(Direction, Damage);

            Health -= Damage;                                //Take the eccess damage straight to the standardZombie
            if (Health <= 0)                            //if health below 0
                Die();                                  //Die

            return Health;
        }
    }
}
