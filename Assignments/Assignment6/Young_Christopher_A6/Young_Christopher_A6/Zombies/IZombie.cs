﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Young_Christopher_A6.Zombies
{
    interface IZombie
    {
        int Health { get; set; }
        bool Alive { get; set; }
        string Name { get; set; }
        bool IsMetal { get; set; }
        void Die();
        int TakeDamage(int Direction, int Damage);
        string Draw();
    }
}
