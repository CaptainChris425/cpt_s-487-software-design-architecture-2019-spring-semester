﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Young_Christopher_A6.Zombies
{
    class ZombieStandard : IZombie
    {
        protected int _health;
        protected bool _alive;
        protected string _name;
        protected bool _isMetal;

        public int Health { get => _health; set => _health = value; }
        public bool Alive { get => _alive; set => _alive = value; }
        public string Name { get => _name; set => _name = value; }
        public bool IsMetal { get => _isMetal; set => _isMetal = value; }

        public ZombieStandard()
        {
            Alive = true;
            Health = 100;
            Name = "Standard Zombie";
            IsMetal = false;
        }

        public void Die()
        {
            Alive = false;
        }

        public string Draw()
        {
            StringBuilder draw = new StringBuilder();
            draw.Append(",=|*.|-");
            draw.Append(Health);
            draw.Append("-,");
            return draw.ToString();
        }

        public int TakeDamage(int Direction, int Damage)
        {
            Health -= Damage;                                //Take the eccess damage straight to the standardZombie
            if (Health <= 0)                            //if health below 0
                Die();                                  //Die

            return Health;
        }
    }
}
