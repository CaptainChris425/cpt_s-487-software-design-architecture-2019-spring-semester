﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Young_Christopher_A6.Zombies;

namespace Young_Christopher_A6.Enemies
{
    abstract class Plant
    {
        protected int _damage;
        protected int Damage { get => _damage; set => _damage = value; }
        protected int _direction;
        protected int Direction { get => _direction; set => _direction = value; }
        public abstract void DealDamage(IZombie zombie);

    }
}
