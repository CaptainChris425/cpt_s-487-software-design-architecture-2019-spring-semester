﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Young_Christopher_A6.Zombies;

namespace Young_Christopher_A6.Enemies
{
    class PeaShooter: Plant
    {
        PeaShooter()
        {
            Damage = 25;
            Direction = Constants.TOP;
        }

        public override void DealDamage(IZombie zombie)
        {
            int damage = zombie.TakeDamage(Direction, Damage);
            while (damage < 0 && zombie.Alive)
            {
                damage = zombie.TakeDamage(Direction, -damage);
            }
        }
    }
}
