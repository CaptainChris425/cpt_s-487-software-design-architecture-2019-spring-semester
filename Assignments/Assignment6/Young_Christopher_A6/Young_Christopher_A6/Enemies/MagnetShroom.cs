﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Young_Christopher_A6.Zombies;

namespace Young_Christopher_A6.Enemies
{
    class MagnetShroom : Plant
    {
        MagnetShroom()
        {
            Damage = 0;
            Direction = Constants.FRONT;
        }

        public override void DealDamage(IZombie zombie)
        {
            if (zombie.IsMetal)
            {
                zombie.TakeDamage(Direction,zombie.Health);
            }
        }
    }
}
